"""
Function name: CLI_Display
A function that displays the stream
a little bit better using colors.
"""
def CLI_Display(text, max):

    length = len(text)

    calc = max - length

    s = []

    for x in range(1, calc):

        s.append(" ")

    formatted = ''.join(s)

    return str(text + formatted)