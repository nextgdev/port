import os, os.path, sys, datetime, random, json
from datetime import date, datetime, time
from babel.dates import get_timezone, UTC, format_date, format_datetime, format_time
from time import gmtime, strftime

"""
This is a class that will select a tweet and will send feedback to
the main server for statistics and optimization for future tweets.
"""
class Twitter_Tweet(object):

    def __init__(self, candidate, text):

        with open("Config/JSON/Config.json") as main:
            config = json.load(main)

        self.IMAGES_METHOD = config['Base']['IMAGES_METHOD']
        self.TIME_ZONE = config['Base']['TIME_ZONE']

        self.candidate = candidate
        self.text = text

    """
    Get random relevant hashtag for candidate
    """
    def get_hashtag(self):

        hashtags = []

        with open('Config/JSON/Candidates/'+self.candidate+'/tweets.json') as data_file:

            data = json.load(data_file)

        for i in data['candidates']['hashtags']:

            hashtags.append( str(i) )

        random_hashtag = random.choice(hashtags)

        return random_hashtag

    """
    Getting the time specific tweets.
    """
    def time_specific(self):

        dt = datetime(int(strftime("%Y")), int(strftime("%m")),
        int(strftime("%d")), int(strftime("%H")), int(strftime("%M")),
        tzinfo=UTC)

        eastern = get_timezone(self.TIME_ZONE)

        timez = int(format_datetime(dt, 'Hmm',
        tzinfo=eastern, locale='en_US')) - 100

        if timez >= 1800 and timez < 0500:

            return "night"

        elif timez >= 0500 and timez < 1159:

            return "morning"

        else:

            return "afternoon"

    """
    This method will return a general tweet if a word connection
    cannot be made
    """
    def general_tweet(self):

        tweets = []

        with open('Config/JSON/Candidates/'+self.candidate+'/tweets.json') as data_file:

            data = json.load(data_file)

        if data['candidates']["##general##"]['tweets']:

            if self.IMAGES_METHOD == "online":

                for i in data['candidates']["##general##"]['tweets']:

                    tweets.append( [str(i['description']), [i['online_images']],
                    int(i['timespecific']), str(i['unique_MD5']) ] )

            else:

                for i in data['candidates']["##general##"]['tweets']:

                    tweets.append( [str(i['description']), [i['images']],
                    int(i['timespecific']), str(i['unique_MD5']) ] )

            select = random.choice(tweets)

            print select

            if select[2] == 1 and select[2]:

                tweet = select[0].replace("{time}", str(self.time_specific()) )
                pictures = random.choice(select[1])

            else:

                tweet = select[0]
                pictures = random.choice(select[1])

            random_image = random.choice(pictures)

            return [tweet, random_image]

    """
    This method will select a random tweet from the tweets.
    """
    def select_tweet(self, connected):

        tweets = []

        with open('Config/JSON/Candidates/'+self.candidate+'/tweets.json') as data_file:

            data = json.load(data_file)

        if data['candidates'][connected]['tweets']:

            if self.IMAGES_METHOD == "online":

                for i in data['candidates'][connected]['tweets']:

                    tweets.append( [str(i['description']), [i['online_images']],
                    int(i['timespecific']), str(i['unique_MD5']) ] )

            else:

                for i in data['candidates'][connected]['tweets']:

                    tweets.append( [str(i['description']), [i['images']],
                    int(i['timespecific']), str(i['unique_MD5']) ] )

            select = random.choice(tweets)

            print select[2]

            if select[2] == 1:

                tweet = select[0].replace("{time}", str(self.time_specific()) )
                pictures = random.choice(select[1])

            else:

                tweet = select[0]
                pictures = random.choice(select[1])

            random_image = random.choice(pictures)

            return [tweet, random_image]

        else:

            return self.general_tweet(self.candidate)

    def send_feedback(self, action, md5):

        """
        This method will be used to send feedback back to the main server.
        """
        print "placement"

    """
    A method that will select a random image from a folder.
    """
    def random_image(self, dir, action):

        random_file = random.choice(os.listdir(dir))

        """
        The move action will ensure that images that have already been tweeted
        will be moved to another folder: "used". To prevent duplicates from
        being tweeted.
        """
        if action == "move":

            file = open(dir + '/' + random_file, 'rb')
            data = file.read()

            shutil.move(dir + '/' + random_file, dir + '/used/' + random_file)

            return data

        else:

            file = open(dir + '/' + random_file, 'rb')
            data = file.read()

            return data