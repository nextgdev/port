"""
A function designed to receive a random image from a folder.
User has the option to decide if the picture should be moved
to the 'pre-defined' folder or not.
"""

def random_image(self, dir, action, folder):

    random_file = random.choice(os.listdir(dir))

    if action == "move":

        file = open(dir + '/' + random_file, 'rb')
        data = file.read()

        shutil.move(dir + '/' + random_file, dir + '/'+folder+'/' + random_file)

        return data

    else:

        file = open(dir + '/' + random_file, 'rb')
        data = file.read()

        return data

