from TwitterAPI import TwitterAPI
from Twitter_Tweet_Class import Twitter_Tweet
from Twitter_Word_Connections_Class import Twitter_Word_Connections
import Functions.candidate
import os, os.path, sys, datetime, random, json

class Base_Twitter(object):

    """
    Setting API variables
    """
    def __init__(self):

        with open("Config/JSON/Config.json") as main:
            config = json.load(main)

        self.config = config

        self.TW_CONSUMER_KEY = config['Base']['TWITTER_API']['CONSUMER_KEY']
        self.TW_CONSUMER_SECRET = config['Base']['TWITTER_API']['CONSUMER_SECRET']
        self.TW_ACCESS_TOKEN_KEY = config['Base']['TWITTER_API']['ACCESS_TOKEN_KEY']
        self.TW_ACCESS_TOKEN_SECRET = config['Base']['TWITTER_API']['ACCESS_TOKEN_SECRET']

        self.TW_ACCOUNTS = config['Base']['ACCOUNTS']
        self.TW_RETWEET_ACCOUNTS = config['Base']['RETWEET_ACCOUNTS']
        self.TW_FOLLOW_ACCOUNTS = config['Base']['REPLY_ACCOUNTS']
        self.IMAGES_METHOD = config['Base']['IMAGES_METHOD']

    def Twitter_API(self):

        api = TwitterAPI(
        self.TW_CONSUMER_KEY,
        self.TW_CONSUMER_SECRET,
        self.TW_ACCESS_TOKEN_KEY,
        self.TW_ACCESS_TOKEN_SECRET
        )

        return api

    """
    Getting the Twitter accounts to follow
    """
    def TW_ACCOUNTS_FOLLOW(self):

        accounts = self.TW_FOLLOW_ACCOUNTS

        return str(accounts)

    """
    A method to check if account in input is an account we want to reply/rt.
    """
    def TW_ANALYSE(self, input):

        loop = self.config['Accounts']

        try:

            if loop[input]:

                return 1
            
            else:

                return 0

        except Exception as b:

            print "Nothing found with:" + str(b)

    """
    A method to upload picture(s) to Twitter.
    """
    def TW_UPLOAD_IMAGE(self, image):

        api = Twitter_API()

        d = api.request('media/upload', None, {'media': image})

        if d.status_code == 200:

            return d.json()['media_id']

        else:

            return False

    """
    A method to upload a video to Twitter.
    """
    def TW_UPLOAD_VIDEO(self, video):

        api = Twitter_API()

        bytes_sent = 0
        total_bytes = os.path.getsize(video)
        file = open(video, 'rb')

        r = api.request('media/upload', {'command':'INIT',
        'media_type':'video/mp4', 'total_bytes':total_bytes})

        if r.status_code < 200 or r.status_code > 299:

            return "Something messed up!"

        else:

            media_id = r.json()['media_id']
            segment_id = 0

            while bytes_sent < total_bytes:

                chunk = file.read(4*1024*1024)
                r = api.request('media/upload', {'command':'APPEND',
                'media_id':media_id, 'segment_index':segment_id}, {'media':chunk})

                segment_id = segment_id + 1
                bytes_sent = file.tell()

            r = api.request('media/upload', {'command':'FINALIZE',
            'media_id':media_id})

            return media_id

    """
    A method to Tweet with all specified info.
    """
    def TW_FINAL(self, candidate, tweet, reply_id, media, media_type):

        api = Twitter_API()

        # Tweeting
        if media and media_type != "online":
            d = api.request('statuses/update', {'status': '@'+candidate+' '
            + tweet, 'in_reply_to_status_id': reply_id, 'media_ids':media})

        else:
            d = api.request('statuses/update', {'status': '@'+candidate+' '
            + tweet, 'in_reply_to_status_id': reply_id})

    """
    A method to select a tweet from the JSON.
    """
    def TW_TWEET(self, candidate, text, reply_id):

        get_tweet = Twitter_Tweet(candidate, text)
        word_connections = Twitter_Word_Connections(candidate)

        connection_made = word_connections.final_check(candidate, text)

        tweet_info = get_tweet.select_tweet(connection_made)
        hashtag = get_tweet.get_hashtag()

        tweet = tweet_info[0] + " " + hashtag
        media = tweet_info[1]

        self.TW_FINAL(candidate, tweet, reply_id, media, self.IMAGES_METHOD)