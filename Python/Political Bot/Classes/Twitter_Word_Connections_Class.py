import os, os.path, sys, datetime, random, json

"""
This is a class that will select a tweet and will send feedback to
the main server for statistics and optimization for future tweets.
"""
class Twitter_Word_Connections(object):

    def __init__(self, candidate):

        self.candidate = candidate

        with open('Config/JSON/Candidates/'+self.candidate+
        '/word_connections.json') as data_file:

            data = json.load(data_file)

        self.data = data

    """
    Get all the hashtags.
    """
    def get_hashtags(self, text):

        print self.tweet_filter(text)

        for x in self.data[self.candidate]['hashtags']:

            print x

    """
    Filtering the tweet and getting rid of unnecessary stuff.
    """
    def tweet_filter(self, text):

        text = [x.strip().lower() for x in text.split(',')
        for x in text.split(' ')]

        new = []

        # Removing certain signs from words (@,.,# etc)
        for b in text:

            new.append(b.replace(".","").replace("@","").replace("#","")
            .replace(":","").replace('"', '').replace(',', '').replace('-', '')
            .replace(' ', '').replace('!', '').replace('?', ''))

        # Getting list of most used English words for filtering them out
        file_name = 'Config/JSON/Candidates/common.txt'

        with open(file_name) as f:

            words = f.read().splitlines()

        refined = []

        for o in new:

            if o not in words and o != str("i"):

                # Making sure no duplicates in returned list
                if o not in refined:

                    refined.append(o)

        return refined

    """
    Check if a certain word is an American state.
    """
    def tweet_filter_states(self, word):

        file_name = 'Config/JSON/Candidates/'+self.candidate+'/word_connections.json'

        with open(file_name) as data_file:

            data = json.load(data_file)

        for d in data[self.candidate]['states_hash']:

            if word.lower() == d.lower():

                return word.lower()

    """
    Check if a word connection can be made
    """
    def word_connections(self, word):

        file_name = 'Config/JSON/Candidates/'+self.candidate+'/word_connections.json'

        with open(file_name) as data_file:

            data = json.load(data_file)

        for e in data[self.candidate]['word_connections']:

            if word in data[self.candidate]['word_connections'][e]['words']:

                return e.lower()

    """
    Final check on word connection and return back definite value to
    Twitter Tweet Class.
    """
    def final_check(self, candidate, text):

        clean_up = self.tweet_filter(text)

        print clean_up

        for x in clean_up:

            if self.word_connections(x):

                return self.word_connections(x)

            if self.tweet_filter_states(x):

                return self.tweet_filter_states(x)

            else:

                return "##general##"
                # Here we need a method that sends feedback to Mother









