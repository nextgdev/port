import formatting
from Classes.Base_Twitter_Class import Base_Twitter
from termcolor import colored

"""
Setting the variables
"""
init = Base_Twitter()
api = init.Twitter_API()
accounts = init.TW_ACCOUNTS_FOLLOW()


"""
Starting the stream
"""
while True:

    try:
        
        """
        Following the accounts set in the JSON config file.
        """
        r = api.request('statuses/filter', {'follow': accounts})

        for item in r.get_iterator():

            if 'text' in item:

                """
                Checking if the screen_name included a presidential candidate.
                """
                if init.TW_ANALYSE( str(item['user']['screen_name']) ) == 1:

                    init.TW_TWEET(item['user']['screen_name'], item['text'], item['id_str'])
                
                else:

                    print colored('[USER ID]', 'red') + " " \
                    + formatting.CLI_Display(item['user']['id_str'], 12) + " " \
                    + colored('[SCREEN NAME]', 'red') + " " \
                    + formatting.CLI_Display(item['user']['screen_name'], 17) + " " \
                    + colored('[DATE]', 'red') + " " + str(item['created_at'])

    except Exception as e:

        if str(e) in item['user']['screen_name']:

            pass

        else:

            print e
            continue
