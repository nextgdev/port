<?php
use App\Company;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    public function run()
    {
        $this->call('CompaniesTableSeeder');

        $this->command->info('Companies table seeded!');
    }

}

class CompaniesTableSeeder extends Seeder {

    public function run()
    {

        Company::create(
            ['company_name' => 'FONU2 INC',
            'ticker' => 'FONU',
            'exchange' => 'QB',
            'company_website' => 'http://fonu2.com/',
            'current_price' => 0.00020]
        );
    }

}