<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('twitter_accounts', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('bot_name');
            $table->string('bot_purpose');

            $table->string('bot_api_oauth_token');
            $table->string('bot_api_oauth_secret');
            $table->string('bot_api_consumer_key');
            $table->string('bot_api_consumer_secret');

            $table->string('company_name')->nullable();
            $table->text('terms')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('twitter_accounts');
	}

}