<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tweets', function($table)
        {
            $table->increments('id');
            $table->integer('user_id');

            $table->string('tweet_message');
            $table->integer('tweet_project');
            $table->integer('tweet_time_specific');
            $table->integer('tweet_count');
            $table->string('tweet_time');
            $table->text('tweet_pictures_online');
            $table->text('tweet_pictures_local');
            $table->string('tweet_md5');
            
            $table->integer('status');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tweets');
	}

}
