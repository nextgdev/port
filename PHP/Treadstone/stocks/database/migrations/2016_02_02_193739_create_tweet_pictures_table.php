<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetPicturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tweet_pictures', function($table)
        {
            $table->increments('id');

            $table->string('tweet_picture_name');
            $table->string('tweet_picture_url');
            $table->string('tweet_picture_local');

            $table->text('tweet_description');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tweet_pictures');
	}

}
