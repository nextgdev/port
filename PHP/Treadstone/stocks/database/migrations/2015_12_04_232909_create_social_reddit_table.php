<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialRedditTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('social_reddit', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('unique_reddit')->index();
            $table->integer('ups');
            $table->string('title');
            $table->string('domain');
            $table->string('reddit');
            $table->string('thumbnail');
            $table->string('url');
            $table->timestamp('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('social_reddit');
	}

}
