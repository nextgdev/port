<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterFollowTweetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tracker_tweets', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('bot_id');

            $table->string('tweet_text');
            $table->integer('tweet_id');
            $table->string('tweet_username');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracker_tweets');
	}

}
