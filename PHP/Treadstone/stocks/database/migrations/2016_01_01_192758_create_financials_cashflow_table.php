<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancialsCashflowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Financials_Cashflow', function($table) {

            $table->increments('id');

            $table->integer('company_id');
            $table->integer('sheet_type');

            $table->string('period_ending');

            $table->float('net_income', 15, 3);

            $table->float('depreciation', 15, 3);
            $table->float('adjustments_to_net_income', 15, 3);
            $table->float('changes_in_accounts_receivables', 15, 3);
            $table->float('changes_in_liabilities', 15, 3);
            $table->float('changes_in_inventories', 15, 3);
            $table->float('changes_in_other_operating_activities', 15, 3);

            $table->float('total_cashflow_from_operating_activities', 15, 3);

            $table->float('capital_expenditures', 15, 3);
            $table->float('investments', 15, 3);
            $table->float('other_cashflow_from_investing_activities', 15, 3);

            $table->float('total_cashflow_from_investing_activities', 15, 3);

            $table->float('dividends_paid', 15, 3);
            $table->float('sale_purchase_of_stock', 15, 3);
            $table->float('net_borrowings', 15, 3);
            $table->float('other_cashflow_from_financing_activities', 15, 3);

            $table->float('total_cashflow_from_financing_activities', 15, 3);

            $table->float('effect_of_exchange_rate_changes', 15, 3);

            $table->float('change_in_cash_and_cash_equivalents', 15, 3);

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Financials_Cashflow');
	}

}
