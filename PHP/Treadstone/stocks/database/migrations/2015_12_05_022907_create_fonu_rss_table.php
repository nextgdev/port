<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFonuRssTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('rss_saves', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->string('unique');
            $table->string('company');
            $table->timestamp('last_modified');
            $table->text('summary');
            $table->string('url')->index();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rss_saves');
	}

}
