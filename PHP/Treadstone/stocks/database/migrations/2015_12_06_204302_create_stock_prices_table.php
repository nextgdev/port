<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('stock_prices', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('source');

            $table->string('symbol')->index();
            $table->string('company');
            $table->string('exchange');
            $table->string('stock_type');

            $table->float('current_price');

            $table->timestamp('last_trade');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stock_prices');
	}

}
