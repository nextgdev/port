<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetWordCombinationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tweet_word_combinations', function($table)
        {
            $table->increments('id');

            $table->integer('tweet_project_id');
            $table->text('tweet_words');
            $table->text('tweet_connect_words')

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tweet_word_combinations');
	}

}
