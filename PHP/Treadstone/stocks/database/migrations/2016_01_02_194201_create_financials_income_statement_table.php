<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancialsIncomeStatementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Financials_IncomeStatement', function($table) {

            $table->increments('id');

            $table->integer('company_id');
            $table->integer('sheet_type');

            $table->string('period_ending');

            $table->float('total_revenue', 15, 3);
            $table->float('cost_of_revenue', 15, 3);

            $table->float('gross_profit', 15, 3);
                $table->float('research_development', 15, 3);
                $table->float('selling_general_and_administrative', 15, 3);
                $table->float('non_recurring', 15, 3);
                $table->float('others', 15, 3);

                $table->float('total_operating_expenses', 15, 3);

            $table->float('operating_income_or_loss', 15, 3);
                $table->float('total_other_income_expenses_net', 15, 3);
                $table->float('earnings_before_interest_and_taxes', 15, 3);
                $table->float('interest_expense', 15, 3);
                $table->float('income_before_tax', 15, 3);
                $table->float('income_tax_expense', 15, 3);
                $table->float('minority_interest', 15, 3);
                $table->float('net_income_from_cont_ops', 15, 3);

                $table->float('disc_ops', 15, 3);
                $table->float('extraordinary_items', 15, 3);
                $table->float('effect_of_accounting_changes', 15, 3);
                $table->float('other_items', 15, 3);

            $table->float('net_income', 15, 3);
            $table->float('pref_stock_and_other_adjustments', 15, 3);
            $table->float('net_income_applicable_to_common_shares', 15, 3);

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Financials_IncomeStatement');
	}

}
