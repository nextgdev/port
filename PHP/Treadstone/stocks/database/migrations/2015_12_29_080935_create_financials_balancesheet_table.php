<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancialsBalancesheetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('Financials_BalanceSheet', function($table) {

            $table->increments('id');

            $table->integer('company_id');
            $table->integer('sheet_type');

            $table->string('period_ending');

            $table->float('assets_current_assets_candcequiv', 15, 3);
            $table->float('assets_current_assets_shorttinvest', 15, 3);
            $table->float('assets_current_assets_netreceiv', 15, 3);
            $table->float('assets_current_assets_inventory', 15, 3);
            $table->float('assets_current_assets_othercurrassets', 15, 3);
            $table->float('assets_current_assets_total', 15, 3);

            $table->float('assets_long_term_investments', 15, 3);
            $table->float('assets_propertyplantequip', 15, 3);
            $table->float('assets_goodwill', 15, 3);
            $table->float('assets_intang_assets', 15, 3);
            $table->float('assets_accum_amortization', 15, 3);
            $table->float('assets_other_assets', 15, 3);
            $table->float('assets_def_longterm_assets', 15, 3);
            $table->float('assets_total_assets', 15, 3);

            $table->float('liabilities_curr_liabilities_acc_pay', 15, 3);
            $table->float('liabilities_curr_short_long_t_debt', 15, 3);
            $table->float('liabilities_curr_other_curr_liabilities', 15, 3);

            $table->float('liabilities_total_curr_liabilities', 15, 3);

            $table->float('liabilities_long_term_debt', 15, 3);
            $table->float('liabilities_other_liabilities', 15, 3);
            $table->float('liabilities_deff_long_term_liability', 15, 3);
            $table->float('liabilities_minority_interest', 15, 3);
            $table->float('liabilities_negative_goodwill', 15, 3);
            $table->float('liabilities_total_liabilities', 15, 3);

            $table->float('stockhequity_misc_stocks_options_warrants', 15, 3);
            $table->float('stockhequity_redeemable_pref_stock', 15, 3);
            $table->float('stockhequity_pref_stock', 15, 3);
            $table->float('stockhequity_comm_stock', 15, 3);
            $table->float('stockhequity_ret_earnings', 15, 3);
            $table->float('stockhequity_treasury_stock', 15, 3);
            $table->float('stockhequity_capital_surplus', 15, 3);
            $table->float('stockhequity_other_stockholder_equity', 15, 3);

            $table->float('stockhequity_total_stockholder_equity', 15, 3);

            $table->float('net_tangible_assets', 15, 3);

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Financials_BalanceSheet');
	}

}
