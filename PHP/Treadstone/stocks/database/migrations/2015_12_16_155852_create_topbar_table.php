<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopbarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('topbar', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('issuedCurrency');
            $table->string('longName');
            $table->float('price');
            $table->float('priceChange1Day');
            $table->float('percentChange1Day');
            $table->string('securityType');
            $table->string('bID');
            $table->string('shortName');
            $table->integer('marketOpen');

            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('topbar');
	}

}
