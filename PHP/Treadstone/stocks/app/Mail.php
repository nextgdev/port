<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inbox';

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


}