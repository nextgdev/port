<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'api_auth';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}