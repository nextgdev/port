<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

    Route::get('/', 'WelcomeController@index');

    /* Crons */
    Route::get('/create_json', 'CronController@create_json');
    Route::get('/is_online', 'CronController@is_online');

    // Testing API with standard value
    Route::get('/4ca1b49056a9cb5a1ed7af6aaed39915/Main.json', 'CronController@test');

    /* API */
    Route::any('/v1/{method}/{value}', 'APIController@get_method');

    /*
        Following routes require auth.
    */

// Grouped routes for authenticated users only
Route::group(['middleware' => 'auth'], function () {

    // Dashboard (related) Routes
    Route::get('/dashboard', 'HomeController@index');
    Route::get('/create_topbar', 'CronController@create_topbar_json');
    Route::post('/feature/add', 'FeatureController@add_feature');
    Route::post('/user/is_online', 'UserController@is_online');

    // Main Terminal Route
    Route::post('/terminal', 'TerminalController@index');

    // User Routes
    Route::any('/profile', 'UserController@edit_profile');
    Route::post('/profile', 'UserController@upload_profile_picture');

    // Financial Routes
    Route::get('/stock/{stock}', 'FinancialController@show_stock');

    // Tweet Admin Routes
    Route::get('/twitter/index', 'TweetController@index');
    Route::post('/twitter/update', 'TweetController@edit');
    Route::get('/twitter/json/tweet/{id}', 'TweetController@get_json');
    Route::get('/twitter/json/categories', 'TweetController@get_categories_json');

    // Admin Routes
    Route::get('/admin/users', 'AdminController@users');
    Route::post('/admin/users/create', 'AdminController@create_user');
    Route::post('/admin/users/delete', 'AdminController@delete_user');
    Route::get('/admin/planning', 'AdminController@planning');
    Route::get('/admin/companies', 'AdminController@companies');
    Route::get('/admin/feeds', 'AdminController@feeds');

    // Social Routes
    Route::get('/inbox', 'MailController@index');
    Route::get('/webrtc', 'ChatController@webrtc');
    Route::post('/connect', 'ChatController@connect');

        // Chat Routes
        Route::post('/encrypted_chat', 'ChatController@send_message');
        Route::get('/encrypted_chat', 'ChatController@get_messages');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
