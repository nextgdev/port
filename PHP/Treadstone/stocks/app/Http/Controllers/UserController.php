<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Hash;
use Auth;
use Session;
use Validator;
use Image;

/**
 * User Controller
 *
 * A User Controller that will deal with methods relating
 * to the User actions on the website.
 *
 * @since Last edited 12th of February, 2016
 *
 */

class UserController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * User editing profile
     *
     * A method that deals with the interaction between the user's profile
     * and the database.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @return string $name Name of the page displayed at top.
     * @return string $message A success/fail message.
     * @return Response
     */

    public function edit_profile(Request $request)
    {

        if ($request->is('profile') && $request->isMethod('post')) {

            $name = $request->input('name');
            $user_id = $request->input('user_id');
            $email = $request->input('email');
            $password = $request->input('password');
            $password2 = $request->input('password_again');
            $feeds = $request->input('feeds');

            if( null !== $request->input('save_personal') ){

                if( $password == $password2 ){

                    $user = User::find($user_id);
                    $user->name = $name;
                    $user->email = $email;
                    $user->password = Hash::make($password);
                    $user->save();

                    return redirect('profile')
                    ->with('name', "Edit Profile")
                    ->with('message', "Profile succesfully updated!");
                }
                else{
                    return redirect('profile')
                    ->with('name', "Edit Profile")
                    ->with('message', "Passwords are not the same!");

                }
            }

            if( null !== $request->input('save_custom') ){

                if( $password == $password2 ){

                    $user = User::find($user_id);
                    $user->feeds = $feeds;
                    $user->password = Hash::make($password);
                    $user->save();

                    return redirect('profile')
                    ->with('name', "Edit Profile")
                    ->with('message', "Profile succesfully updated!");
                }
                else{
                    return redirect('profile')
                    ->with('name', "Edit Profile")
                    ->with('message', "");

                }
            }

        }
        else{

            return view('users.profile')
            ->with('name', "Edit Profile")
            ->with('message', NULL);
        }

    }

    /**
     * Online update
     *
     * A method that upon request will update the user's online status to 1.
     * i.e. the user is currently online.
     *
     * @param Auth()
     */
    
    public function is_online(){

        $user = User::find(Auth::user()->id);

        $user->is_online = 1;

        $user->save();
    }

    /**
     * User upload profile picture
     *
     * A method that allows the authenticated user to update/upload its own
     * profile picture.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @return Redirect
     * @return Session Flash message
     */

    public function upload_profile_picture(Request $request){

        $file = array('image' => $request->file('profile_picture'));

        $rules = array('image' => 'required',);

        $validator = Validator::make($file, $rules);

        if ( $validator->fails() ) {

            return redirect('profile')->withInput()->withErrors($validator);
        }

        else {

            if ( $request->file('profile_picture')->isValid() ) {

                $destinationPath = public_path("editor/images/users/");

                $extension = $request->file('profile_picture')->getClientOriginalExtension();

                $fileName = "avatar_" . Auth::user()->id . "." . $extension;

                $img = Image::make($request->file('profile_picture'))->resize(100, 100);
                $img->save($destinationPath . $fileName);

                $user = User::find(Auth::user()->id);

                $user->profile_image = $fileName;

                $user->save();

              Session::flash('success', 'Upload successfully');

              return redirect('profile');
            }

            else {
              // sending back with error message.
              Session::flash('error', 'uploaded file is not valid');
              return redirect('profile');
            }

          }

        }

}