<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DateTime;
use Auth;
use App\Feature;

/**
 * Feature Controller
 *
 * A Feature Controller that will deal with methods relating
 * to the features on the website.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class FeatureController extends Controller {

    /**
     * Adding a feature
     *
     * A method that inserts a submitted feature from the modal in the back-end
     * to the database via JSON.
     *
     */
    public function add_feature(Request $request){

        $json_a = json_decode($request->input('data'), true);

        if( !empty( $json_a['feature'] ) ){

            $feature = new Feature;

            $feature->user_id = Auth::user()->id;
            $feature->feature = $json_a['feature'];
            $feature->status = 0;

            $feature->save();

        }
    }

}