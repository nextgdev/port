<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DateTime;
use Auth;

/**
 * Terminal Controller
 *
 * A Terminal controller will retrieve all the values that are available
 * to the authenticated user. A terminal is essentially the same as what users
 * of the Bloomberg Terminal have presented; a bunch of shortcuts via a terminal.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class TerminalController extends Controller {

    /**
     * Terminal checking commands
     *
     * A method that first checks if the commands are allows, possible and
     * present in the database. If they are, it'll redirect the authenticated
     * user to the desired page.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @return Redirect A redirect to the page connected to the terminal command
     */
    
    public function index(Request $request){

        $terminal_value = $request->input('terminal_value');

        $terminal = DB::table('terminal')->get();

        foreach ($terminal as $key) {

            if( $key->command == $terminal_value ) {

                return redirect($key->redirect);
            }
        }

        return redirect('/dashboard');
    }

}