<?php namespace App\Http\Controllers;

use DB;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

    /**
     * Show index
     *
     * A method that sends all the necessary information to the index view
     * and sends along variables containing various objects.
     *
     * @return string $name Name of the page displayed at top.
     * @return object $social Returns an object containing Reddit threads.
     * @return object $rss_feeds Returns all the information for rendering RSS.
     * @return object $tweets Returns all the tweets collected by Twitter script.
     * @return Response
     */

	public function index()
	{
        $social = DB::table('social_reddit')->orderBy('id', 'desc')->take(25)->get();
        $rss_feeds = DB::table('rss_saves')->orderBy('updated_at', 'desc')->take(25)->get();
        $tweets = DB::table('tracker_tweets')->orderBy('created_at', 'desc')->take(25)->get();

		return view('index')
        ->with('name', "Dashboard")
        ->with('social', $social)
        ->with('rss_feeds', $rss_feeds)
        ->with('tweets', $tweets);
	}

}
