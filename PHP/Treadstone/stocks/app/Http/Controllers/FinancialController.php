<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Company;

/**
 * Financial Controller
 *
 * A Financial Controller that will deal with methods relating
 * to the Financial actions on the website.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class FinancialController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show stock ticker
     *
     * A method that returns an object with all information about the company
     * from the database.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @param string $stock Stock ticker of the company.
     * @return string $name Name of the page displayed at top.
     * @return Response
     */

    public function show_stock(Request $request, $stock)
    {

        $company = Company::where('ticker', $stock)->firstOrFail();

        return view('company/index')
        ->with('name', $company->company_name)
        ->with('company', $company);
    }
}