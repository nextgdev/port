<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Planning;
use App\Company;
use App\Feed;
use DateTime;
use Auth;
use Hash;

/**
 * Admin Controller
 *
 * An Admin Controller that will deal with methods relating
 * to the Admin panel in the backend.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class AdminController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * A get method for all users
     *
     * A method that returns a list of all the users currently in
     * the database
     *
     * @return obj Returns an object containing all the users.
     * @return string $name Name of the page displayed at top.
     * @return string $message An empty message.
     * @return Response
     */

    public function users()
    {

        $users = User::all();

        return view('admin.users')
        ->with('name', "Administrate Users")
        ->with('message', NULL)
        ->with('users', $users);

    }

    /**
     * A method for creating users
     *
     * A method that allows an admin to create users from the Back-End.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @return string $name Name of the page displayed at top.
     * @return function Returns the return to page function upon success.
     * @return Response
     */

    public function create_user(Request $request)
    {
        $users = User::all();

        // Checking the origin of the request page
        if ( $request->is('admin/users/*') && $request->isMethod('post') ) {

            // Setting variables
            $name = $request->input('create_name');
            $email = $request->input('create_email');
            $password = $request->input('create_password');

            // Creating new user object
            $user = new User;

            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);

            // Saving/Inserting new user in to the database
            $user->save();

            // If succesful, return back to User Admin page with success mess.
            return redirect('admin/users')
            ->with('name', "Administrate Users")
            ->with('message', "User " . $name . " added!")
            ->with('users', $users);
        }
        else{

            // When someone went wrong, return back to User Admin page with
            // failure message.
            return ('admin.users')
            ->with('name', "Administrate Users")
            ->with('message', NULL)
            ->with('users', $users);
        }
    }

    /**
     * A method for deleting users
     *
     * A method that allows an admin to delete users from the Back-End.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @return string $name Name of the page displayed at top.
     * @return function Returns the return to page function upon success.
     * @return Response
     */

    public function delete_user(Request $request)
    {   
        // IDs of Admins.. ToDo: retrieve from DB.
        $master = array(1,2);

        // Decoding the JSON retrieved from Back-End
        $json_a = json_decode($request->input('data'), true);

        // Checking the origin of the request page
        if ( $request->is('admin/*') && $request->isMethod('post') && $request->ajax() ) {

            // Check to see if someone isn't trying to delete an admin.
            if(  !in_array($json_a['delete_id'], $master)  ){

                // Delete User.
                User::destroy($json_a['delete_id']);

                // Return with Json
                return json_encode($json_a['delete_id'], 128);
            }
        }
        else{

            $users = User::all();

            return view('admin.users')
            ->with('name', "Administrate Users")
            ->with('message', NULL)
            ->with('users', $users);
        }
    }

    /**
     * A get method for all planning
     *
     * A method that returns a list of all the planning activities currently in
     * the database
     *
     * @return string $name Name of the page displayed at top.
     * @return object $plannings Returns an object containing all the plans.
     * @return Response
     */

    public function planning(){

        $plannings = Planning::get();

        return view('admin.planning')
        ->with('name', "Change Planning")
        ->with('plannings', $plannings);
    }

    /**
     * A get method for all the companies
     *
     * A method that returns a list of all the companies currently in
     * the database
     *
     * @return string $name Name of the page displayed at top.
     * @return object $companies Returns an object containing all the companies.
     * @return Response
     */

    public function companies(){

        $companies = Company::get();

        return view('admin.companies')
        ->with('name', "Administrate Companies")
        ->with('companies', $companies);
    }

    /**
     * A get method for all the RSS feeds
     *
     * A method that returns a list of all the RSS feeds currently in
     * the database
     *
     * @return string $name Name of the page displayed at top.
     * @return object $feeds Returns an object containing all the RSS feeds.
     * @return Response
     */

    public function feeds(){

        $feeds = Feed::get();

        return view('admin.feeds')
        ->with('name', "Administrate Feeds")
        ->with('feeds', $feeds);
    }
}