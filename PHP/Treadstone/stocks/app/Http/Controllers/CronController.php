<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DateTime;
use App\User;

/**
 * Cron Controller
 *
 * A Cron Controller that deals with everything related to crons.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class CronController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Creating Stock data JSON
     *
     * A method outputs a JSON file necessary to display the stock chart in the
     * dashboard of the Back-End.
     *
     * @return void
     */

    public function create_json()
    {

        // Open the file to get existing content
        $stock_json = fopen(public_path() . "/charts/FONU.json", "w")
        or die("Unable to open file!");

        // Retrieve prices from the database where the symbol is FONU
        $prices = DB::table('stock_prices')->where('symbol', 'FONU')->get();

        $price_array = array();

        // Create the structure and values of JSON file
        foreach ($prices as $price) {

            $date = new DateTime($price->last_trade);

            $date->format("U");

            $new = array( $date->getTimestamp() * 1000, floatval($price->current_price) );

            array_push( $price_array, $new );

        }

        $price_array = json_encode($price_array);

        // Write JSON file
        fwrite($stock_json, $price_array);
        fclose($stock_json);

    }

    /**
     * Creating the topbar with stock prices
     *
     * A method that returns JSON values with companies and their stock prices
     * which allows the back-end to interpret them and display them.
     *
     * @return array Returns the prices in JSON format.
     */

    public function create_topbar_json(){

        return json_encode(DB::table('topbar')->get());
    }

    /**
     * Updates the user's online status
     *
     * A method that updates the user's status as not online
     *
     * @return void
     */

    public function is_online(){

        DB::table('users')->update(array('is_online' => 0));
    }

}