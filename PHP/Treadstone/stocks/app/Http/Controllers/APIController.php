<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Api;
use DB;
use Illuminate\Http\Request;

/**
 * API Controller
 *
 * This API Controller is used to interpret any outside
 * input and/or to output certain commands created by the
 * back-end.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class APIController extends Controller {

    /**
     * The Get method for outside interaction
     *
     * A method that interprets the commands sent by
     * the external scripts. i.e. Political bot.
     *
     * @param string $method A method/command used by external script.
     * @param string|array $value The value sent along with the method.
     * @return bool|string If may return a method if command exist, else 0.
     */

	public function get_method($method, $value)
	{

        switch ($method) {

            case "auth":
                return $this->auth($value);
                break;

            default:
                return 0;
        }

    }

    /**
     * Authentication method.
     *
     * This method checks if the outside force/script should get
     * access to the database of the back-end. And will return a boolean
     * upon success or failure.
     *
     * @param array $input An array with authentication values, like REST.
     * @return bool Will return a bool, either allowed (1) or not (0).
     */
    
    public function auth($input){

        $check = json_decode($input);

        $api = DB::table('api_auth')
        ->where('mother_key', $check->MOTHER_KEY)
        ->where('mother_secret', $check->MOTHER_SECRET)
        ->where('mother_unique', $check->MOTHER_UNIQUE)
        ->count();

        return $api;
    }

}
