<?php namespace App\Http\Controllers;

use DB;
use App\Tweet;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

/**
 * Tweet Controller
 *
 * A Tweet Controller that will deal with methods relating
 * to the Twitter actions on the website.
 *
 * @since Last edited 25nd of February, 2016
 *
 */

class TweetController extends Controller {

    /**
     * Show all tweets
     *
     * A method that returns all the tweets in the database. Later used for
     * generating a JSON file used by external bots.
     *
     * @return object $name Name of the page displayed at top.
     * @return object $tweets An object containing all tweets from the DB.
     * @return Response
     */

	public function index()
	{

		$get_tweets = Tweet::all();

		return view('twitter/index')
        ->with('name', 'Tweets Index')
        ->with('tweets', $get_tweets);
	}

    /**
     * Get JSON for Tweet
     *
     * A method that is created especially for the back-end dealing with 
     * the actions involved with editing/deleting/adding tweets. The Jquery
     * front-end operational side needs improvement.
     *
     * @param string $id The ID of the tweet retrieved from the front-end.
     * @return object $get_tweet Get an object of the tweet with requested ID.
     */

	public function get_json($id)
	{

		$get_tweet = Tweet::find($id);

		return json_encode($get_tweet, 128);
	}

    /**
     * Get categories for the tweets
     *
     * A method that retrieves the categories the back-end allows tweets to be
     * categorized in. I.e. the trial purpose for the tweets are American pres-
     * idential candidates.. every candidate has its own category and it's own
     * individual tweets
     *
     * @return array $categories Returning a JSON object with categories
     */

	public function get_categories_json()
	{

		$categories = DB::table('tweet_projects')->get();

		return json_encode($categories, 128);
	}

	/**
	 * EXPERIMENTAL [WIP]
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function edit(Request $request)
	{

		$data = $request->input('data');
		$json_a = json_decode($data, true);

		$file = fopen (public_path() . "/charts/test.json", "w");
		fwrite($file, $json_a['tweet_text']);
		fwrite($file, $json_a['tweet_keywords']);
		fwrite($file, $json_a['tweet_category']);
		fwrite($file, $json_a['tweet_image']);
		fclose ($file);  
	}

}
