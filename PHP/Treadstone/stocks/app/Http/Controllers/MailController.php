<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DateTime;
use App\Mail;
use Auth;

/**
 * Mail Controller [WIP]
 *
 * A Mail Controller that will deal with all the incoming and outgoing
 * internal mail between admins.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class MailController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the inbox view to the user.
     *
     * @return Response
     * @return string $name Name of the page displayed at top.
     * @return object $mails Returns all the inboxed mails for the user.
     * @return Response
     */
    
    public function index()
    {
        $name = 'Inbox';

        $all_mail = DB::table('inbox')->where('receiver_id', Auth::user()->id)->get();

        return view('mail/inbox')
        ->with('name', $name)
        ->with('mails', $all_mail);
    }

}