<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DateTime;
use Auth;
use Hash;
use App\User;

/**
 * Chat Controller
 *
 * A Chat Controller that will deal with methods relating
 * to the Chat functionality on the website.
 *
 * @since Last edited 22nd of February, 2016
 *
 */

class ChatController extends Controller {

    /**
     * Displaying the WebRTC page
     *
     * A method that creates a display/view for the chat.
     *
     * @return string $name Name of the page displayed at top.
     * @return Response
     */

    public function webrtc(Request $request){

        return view('webrtc/index')
        ->with('name', 'WebRTC');
    }

    /**
     * Send a message
     *
     * A method that inserts a chat message in to the database
     *
     * @param string $request Standard Laravel way of dealing with requests.
     */

    public function send_message(Request $request){

        $json_a = json_decode($request->input('data'), true);

        if( !empty( $json_a['message'] ) ){

            $exist_chat = DB::table('chat')
            ->where('identifier', $json_a['identifier'])
            ->get();

            $identifier = Hash::make(Auth::user()->name . $json_a['user_id']);

            if( strlen($exist_chat[0]->identifier) == 0 ){

                DB::table('chat')->insert(
                    ['user_id' => $json_a['user_id'],
                     'message' => $json_a['message'],
                     'identifier' => $identifier,
                     'receiver_id' => $json_a['receiver_id'],
                     'created_at' => date("Y-m-d H:i:s"),
                     'updated_at' => date("Y-m-d H:i:s")
                    ]
                );

            }
            else{

                $last = DB::table('chat')
                ->where('identifier', $json_a['identifier'])
                ->orderBy('id', 'desc')
                ->first();

                $last_date = explode(" ", $last->created_at);

                if( date("Y-m-d") > $last_date ){

                    $chat_id = $last->chat_id+1;
                }
                else{

                    $chat_id = $last->chat_id;
                }

                DB::table('chat')->insert(
                    ['user_id' => $json_a['user_id'],
                     'message' => $json_a['message'],
                     'message_id' => $last->message_id+1,
                     'chat_id' => $chat_id,
                     'identifier' => $exist_chat[0]->identifier,
                     'receiver_id' => $json_a['receiver_id'],
                     'created_at' => date("Y-m-d H:i:s"),
                     'updated_at' => date("Y-m-d H:i:s")
                    ]
                );
            }

        }
    }

    /**
     * Getting all messages
     *
     * A method that gets all the messages of a chat between the identifier
     * and the authenticated user.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @return array $data Returns the messages in JSON.
     */

    public function get_messages(Request $request){

        $data = DB::table('chat')
        ->where('identifier', $request->input('id'))
        ->orderBy('id', 'desc')
        ->first();

        $username = User::find($data->user_id)->name;

        $data->username = $username;

        return json_encode($data, 128);
    }

    /**
     * Connecting two users
     *
     * A method that connects the authenticated user with another user that is
     * online and willing to chat to the other user.
     *
     * @param string $request Standard Laravel way of dealing with requests.
     * @return array $ret Returns a JSON object with chat information.
     */

    public function connect(Request $request){

        $data = $request->input('data');
        $json_a = json_decode($data, true);

        $count = DB::table('chat')
        ->where('user_id', $json_a['invitee'])
        ->where('receiver_id', $json_a['user_id'])
        ->count();

        if( $count > 0 ){

            $ret = DB::table('chat')
            ->where('user_id', $json_a['invitee'])
            ->where('receiver_id', $json_a['user_id'])
            ->orderBy('id', 'desc')
            ->first();

            return json_encode($ret, 128);
        }
        else{

            $identifier = Hash::make( Auth::user()->name . $json_a['user_id'] );

            $id = DB::table('chat')->insertGetId(
                    ['user_id' => Auth::user()->id,
                     'message' => "[CHAT STARTED]",
                     'message_id' => 0,
                     'chat_id' => 0,
                     'identifier' => $identifier,
                     'receiver_id' => $json_a['invitee'],
                     'created_at' => date("Y-m-d H:i:s"),
                     'updated_at' => date("Y-m-d H:i:s")
                    ]
                );

             $ret = DB::table('chat')
            ->where('id', $id)
            ->first();

            return json_encode($ret, 128);
        }

    }

}