<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feeds';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

}