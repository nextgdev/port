<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use App;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{

        $plannings = App\Planning::distinct()->select('stage')
            ->groupBy('stage')->get();

        $companies = DB::table('companies')->orderBy('company_name', 'asc')->get();
        $online_users = App\User::where('is_online', '>', 0)->get();

        view()->composer('layouts.master', function ($view) use ($companies,
            $plannings, $online_users) {

            $view->with('companies', $companies);
            $view->with('plannings', $plannings);
            $view->with('online_users', $online_users);
        });
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
