import mysql.connector, json,requests, urllib, urllib2
import time, datetime, os
from yahoo_finance import Share

class Treadstone_Stocks_DB(object):

    def __init__(self, user, database, password):

        self.cnx = mysql.connector.connect(user=user, database=database, password=password)
        self.db = self.cnx.cursor()

    """
        A method that checks whether there's an actual connection with a
        database.

        It will return True or False depending on the outcome of the connection.
    """

    def check_connection(self):

        self.db.execute("SELECT VERSION()")
        results = self.db.fetchone()

        if results:
            return True
        else:
            return False

        self.db.close()
        self.cnx.close()

    """
        A method that retrieves the current stock price for every stock
        in the database.
    """

    def update_stock_price(self):

        if self.check_connection() != True:

            print "Couldn't make a connection with the database."

        else:

            query = ("SELECT * FROM companies")
            self.db.execute(query)
            stocks_list = self.db.fetchall()
            columns = self.db.description

            for x in stocks_list:

                request_headers = {
                "Accept-Language": "en-US,en;q=0.5",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "Referer": "http://finance.yahoo.com/",
                "Connection": "keep-alive"
                }

                yahoo = Share(x[2])
                exchange = yahoo.get_stock_exchange()

                request = urllib2.Request("http://finance.yahoo.com/webservice/v1/symbols/" +
                x[2] +"/quote?format=json&view=detail", headers=request_headers)
                contents = urllib2.urlopen(request).read()

                content_fix = contents.replace("// ", "")

                output = open('stock_price_y.json','wb')
                output.write(content_fix)
                output.close()

                with open("stock_price_y.json") as json_file:
                    json_data = json.load(json_file)

                for s in json_data['list']['resources']:

                    price_up = s['resource']['fields']['change']
                    price_perc_up = s['resource']['fields']['chg_percent']

                    time_now = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')).encode('utf-8')
                    curr_price = float(s['resource']['fields']['price'])

                    query = ("UPDATE companies SET current_price = (%s)," +
                    "updated_at = (%s), exchange = (%s), price_up = (%s), "+
                    "price_percentage_up = (%s) WHERE id = (%s)")
                    self.db.execute(query, (curr_price, time_now, exchange, price_up, price_perc_up, x[0]))

                    self.cnx.commit()

            time.sleep(5)

            self.db.close()
            self.cnx.close()

            os.remove("stock_price_y.json")

    def update_top_bar(self):

        request_headers = {
        "Accept-Language": "en-GB,en;q=0.8,en-US;q=0.6,nl;q=0.4",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36",
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Referer": "http://www.bloomberg.com/markets/components/data-drawer",
        "Connection": "keep-alive"
        }

        request = urllib2.Request("http://www.bloomberg.com/markets/api/data-dashboard/tileset/overview", headers=request_headers)
        contents = urllib2.urlopen(request).read()

        output = open('information_top.json','wb')
        output.write(contents)
        output.close()

        with open("information_top.json") as json_file:
            json_data = json.load(json_file)

        for x in json_data['fieldDataCollection']:

            price = x['price']
            priceChange1Day = x['priceChange1Day']
            percentChange1Day = x['percentChange1Day']
            marketOpen = x['marketOpen']

            bID = x['id']
            current_date = datetime.datetime.now()

            query = ("UPDATE topbar SET price = (%s), priceChange1Day = (%s), percentChange1Day = (%s), marketOpen = (%s), updated_at = (%s) WHERE bID = (%s)")
            self.db.execute(query, (price, priceChange1Day, percentChange1Day, marketOpen, current_date, bID))

            self.cnx.commit()