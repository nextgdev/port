import mysql.connector, json,requests, urllib, urllib2
import time, datetime, os, feedparser
from TwitterAPI import TwitterAPI

class Treadstone_Company_GetInfo(object):

    def __init__(self, user, database, password):

        self.cnx = mysql.connector.connect(user=user, database=database, password=password)
        self.db = self.cnx.cursor()

    """
        A method that checks whether there's an actual connection with a
        database.

        It will return True or False depending on the outcome of the connection.
    """

    def check_connection(self):

        self.db.execute("SELECT VERSION()")
        results = self.db.fetchone()

        if results:
            return True
        else:
            return False

        self.db.close()
        self.cnx.close()

    """
        This is essentially a method that will create a bot, a bot that will
        track relevant information about a specific company on Twitter.
    """

    def twitter_track(self, company):

        if self.check_connection() != True:

            print "Couldn't make a connection with the database."

        elif len(company) < 1:

            print "Please enter a company to follow"

        else:

            query = ("SELECT * FROM twitter_accounts WHERE company_name = '" + company + "' AND bot_purpose = 'tracker'")
            self.db.execute(query)

            accounts = self.db.fetchall()

            for x in accounts:

                ACCESS_TOKEN_KEY = x[3]
                ACCESS_TOKEN_SECRET = x[4]
                CONSUMER_KEY = x[5]
                CONSUMER_SECRET = x[6]

                terms = x[8]

            api = TwitterAPI(CONSUMER_KEY,
                CONSUMER_SECRET,
                ACCESS_TOKEN_KEY,
                ACCESS_TOKEN_SECRET)

            while True:

                try:

                    r = api.request('statuses/filter', {'track': terms})

                    for item in r.get_iterator():

                        if 'text' in item:

                            database_add = ("INSERT INTO tracker_tweets (bot_id, tweet_text, tweet_id, tweet_username, created_at, updated_at) VALUES (%s, %s, %s, %s, %s, %s)")

                            tweet_text = item['text'].encode('utf-8')
                            tweet_id = item['id']
                            tweet_username = item['user']['screen_name'].encode('utf-8')
                            current_date = datetime.datetime.now()

                            self.db.execute(database_add, (x[0], tweet_text, tweet_id, tweet_username, current_date, current_date))

                            self.cnx.commit()

                    self.db.close()
                    self.cnx.close()

                except Exception as e:

                    print e

                    continue

    def get_rss(self):

        if self.check_connection() != True:

            print "Couldn't make a connection with the database."

        else:

            """
            Only thing that is left is retrieving feeds from a database based
            on user's preferences.
            """

            feed = feedparser.parse('http://fonu2.com/feed/')

            for x in feed['entries']:

                company = "fonu"
                title = x.title.encode('utf-8')
                summary = x.summary.encode('utf-8')
                date = datetime.datetime.strptime(x.updated.replace(" +0000", ""), '%a, %d %b %Y %H:%M:%S')
                url = x.link.encode('utf-8')

                query = ("SELECT COUNT(1) FROM rss_saves WHERE title = (%s) AND url = (%s)")
                self.db.execute(query, (title, url))

                if self.db.fetchone()[0]:

                    continue

                else:

                    add = ("INSERT INTO rss_saves (title, company, created_at, updated_at, summary, url) VALUES (%s, %s, %s, %s, %s, %s)")
                    self.db.execute(add, (title, company, date, date, summary, url))

                    self.cnx.commit()

                    print "Added: " + title

            self.db.close()
            self.cnx.close()