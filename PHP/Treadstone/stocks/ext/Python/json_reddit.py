# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json,requests, urllib, urllib2, mysql.connector, time, datetime, os

sub = ['finance', 'investing', 'investmentclub', 'stockmarket', 'pennystocks', 'economy']

for k in sub:

    ts = time.time()

    cnx = mysql.connector.connect(user='root', database='stocks', password='xxxxxx')
    cursor = cnx.cursor()

    while True:

        try:
            request_headers = {
            "Accept-Language": "en-US,en;q=0.5",
            "User-Agent": "Mozilla/5.0 (by /u/bradley34) (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Referer": "https://www.reddit.com/r/" + k,
            "Connection": "keep-alive"
            }

            request = urllib2.Request("https://www.reddit.com/r/" + k + ".json", headers=request_headers)
            contents = urllib2.urlopen(request).read()

            output = open(k + '.json','wb')
            output.write(contents)
            output.close()

            break

        except urllib2.HTTPError, err:

            if err.code == 503:

               continue

            else:

                print err.code

    with open(k + ".json") as json_file:
        json_data = json.load(json_file)

    data = json_data['data']['children']

    for x in data:

        title = str(x['data']['title'].replace(u"\u2018", "'").replace(u"\u2019", "'").replace(u"\u2013", "-").replace(u"\u201c", '"').replace(u"\u201d", '"')).encode('utf-8')
        id = str(x['data']['name']).encode('utf-8')
        reddit = str(x['data']['subreddit_id']).encode('utf-8')
        domain = str(x['data']['domain']).encode('utf-8')
        thumb = str(x['data']['thumbnail']).encode('utf-8')
        url = str(x['data']['url']).encode('utf-8')
        ups = str(x['data']['ups']).encode('utf-8')
        times = str(datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')).encode('utf-8')

        query = ("SELECT COUNT(1) FROM social_reddit WHERE unique_reddit = (%s) AND domain = (%s)")
        cursor.execute(query, (id, domain))

        if cursor.fetchone()[0] or x['data']['stickied'] == True:

            continue

        else:

            add = ("INSERT INTO social_reddit (unique_reddit, ups, title, domain, thumbnail, url, reddit, nice_name, created_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")
            cursor.execute(add, (id, ups, title, domain, thumb, url, reddit, k, times))

            cnx.commit()

            print "Added: " + title

    cursor.close()
    cnx.close()

    os.remove(k + ".json")

    print "Time for Sleep"

    time.sleep(300)

