import sys
from Classes.Database_Stocks_Class import Treadstone_Stocks_DB
from Classes.Database_Company_GetInfo_Class import Treadstone_Company_GetInfo

"""

    This run.py is used for crons. It contains the classes needed
    for updating information needed on the website.

"""

DB_USERNAME = "root"
DB_DATABASE = "stocks"
DB_PASSWORD = "xxxxxx"

if len(sys.argv) > 1:

    if sys.argv[1] == "update_rss":

        rss = Treadstone_Company_GetInfo(DB_USERNAME, DB_DATABASE, DB_PASSWORD)
        rss.get_rss()

    elif sys.argv[1] == "track":

        twitter = Treadstone_Company_GetInfo(DB_USERNAME, DB_DATABASE, DB_PASSWORD)
        twitter.twitter_track("FONU")

    elif sys.argv[1] == "update_stocks":

        update_stocks = Treadstone_Stocks_DB(DB_USERNAME, DB_DATABASE, DB_PASSWORD)
        update_stocks.update_stock_price()

    elif sys.argv[1] == "update_top_bar":

        update_topbar = Treadstone_Stocks_DB(DB_USERNAME, DB_DATABASE, DB_PASSWORD)
        update_topbar.update_top_bar()

    else:

        print "Please use one of the following functions:"
        print "1) 'update_rss' [used to update the RSS feeds]"
        print "2) 'track' [will initialize a Twitter follow bot"
        print "3) 'update_stocks' [will update all stock prices in the database]"

else:

    print "Please use one of the following functions:"
    print "1) 'update_rss' [used to update the RSS feeds]"
    print "2) 'track' [will initialize a Twitter follow bot"
    print "3) 'update_stocks' [will update all stock prices in the database]"



