@extends('layouts.master')

@section('header_javascript')

@stop

@section('content')

                <div class="window">
                    <div class="actionbar">
                        <div class="pull-left">
                            <ul class="ext-tabs">
                                <li class="active">
                                    <a href="#content-tab-1">Administrate Feeds</a>
                                </li>
                            </ul><!-- End .ext-tabs -->
                        </div>
                        <div class="pull-right">
                        </div>
                    </div><!-- End .actionbar-->
                    <div class="tab-content">
                        <div id="content-tab-1" class="tab-pane active">
                            <div class="inner-padding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Company</th>
                                            <th scope="col">Url</th>
                                            <th scope="col">Clicks</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($feeds as $feed)
                                        <tr>
                                            <td>{{ $feed->id }}</td>
                                            <td>{{ $feed->company }}</td>
                                            <td>{{ $feed->url }}</td>
                                            <td>{{ $feed->clicks }}</td>
                                            <td><a href="#">Edit</a> | <a href="#">Delete</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                        </div>
                </div>
@stop