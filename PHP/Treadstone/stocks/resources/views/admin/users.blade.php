@extends('layouts.master')

@section('header_javascript')

@stop

@section('content')

    <div class="inner-padding">

<?php if( strlen($message) > 0 ){?>

<div class="alert alert-block alert-success alert-inline-top alert-dismissable">

    <button type="button" class="close" data-dismiss="alert">×</button>
    <h4>Message:</h4>
    {{ $message }}

</div>
<?php } ?>
        <section class="col-md-6">

                                <!-- New widget -->

                                <div class="widget">
                                    <header>
                                        <h2>Add a new User</h2>
                                    </header>
                                    <div>

                                        <div class="inner-padding">
                                            <div class="subheading">
                                <form class="form-horizontal" role="form" id="edit-profile" method="POST" action="{{ url('admin/users/create') }}">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Name</label>
                                    </div>
                                    <div class="col-sm-9">

                                            <input required class="form-control" type="text" placeholder="Name" name="create_name">

                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>E-Mail</label>
                                    </div>
                                    <div class="col-sm-9">

                                            <input required class="form-control" type="email" placeholder="Your E-mail" name="create_email">

                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Password (automatically generated)</label>
                                    </div>
                                    <div class="col-sm-9">

                                            <input required class="form-control" type="text" value="<?php echo rand(3213213213213, 329839812398291839); ?>" name="create_password">
                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">

                                    </div>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        <input type="submit" class="btn btn-primary" name="save_custom" value="Add New User">
                                    </div>
                                </div>
                                </form>
                                        </div>
                                    </div>
                                </div><!-- End .widget -->

                           </section>

                          <section class="col-md-6">

                                 <!-- New widget -->

                                <div class="widget">
                                    <header>
                                        <div class="pull-left">
                                            <h2>Current Users in Treadstone</h2>
                                        </div>
                                        <div class="pull-right">
                                        <h2 id="count">{{ App\User::count() }}</h2>
                                        </div>
                                    </header>
                                    <div>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">E-mail address</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($users as $user)
                                                <tr id="user_{{ $user->id }}">
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td><a href="#">Edit</a> | <a href="javascript:void(0);" class="bootbox-confirm-trigger" data-id="{{ $user->id }}" data-name="{{ $user->name }}">Delete</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- End .widget -->

                                <!-- New widget -->

                                <div class="widget">
                                    <header>
                                        <div class="pull-left">
                                            <h2>Horizontal scrollbar</h2>
                                        </div>
                                        <div class="pull-right"></div>
                                    </header>
                                    <div>
                                        <div class="scrollbar-x"><div class="viewport"><div class="overview" style="left: 0px;">
                                            <div class="inner-padding">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempus, eros a porta fringilla, erat turpis mollis elit,
                                            eu fermentum orci turpis a felis. Nulla fermentum faucibus bibendum. Aenean a diam purus. Morbi pulvinar lacus vitae euismod laoreet.
                                            Integer laoreet sem id sapien posuere auctor. Sed mauris ipsum, scelerisque nec euismod sed, eleifend non erat.
                                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti.
                                            Aliquam vitae cursus enim, molestie aliquam nisl. Proin semper molestie tellus, vitae euismod ante placerat vel.
                                            Vestibulum mattis fringilla ipsum, ut volutpat lacus bibendum eu. Sed eu ipsum euismod, tincidunt magna et, rutrum nisl.
                                            Nullam vel diam et nulla iaculis varius vitae ac velit. Aliquam ac massa adipiscing, ultricies purus eu, vehicula tortor.
                                            Ut ornare malesuada magna a aliquam. Quisque et tellus eget augue tincidunt lobortis in id est. Donec hendrerit, urna non
                                            vulputate aliquam, erat lectus faucibus justo, at interdum velit mauris vel tellus. Nullam aliquet vestibulum nibh, at imperdiet
                                            mauris feugiat sed. Maecenas tincidunt enim neque, quis ultrices felis ullamcorper in. Suspendisse potenti.
                                            Morbi laoreet elementum urna. Suspendisse tincidunt eleifend mauris, congue tempor turpis. Donec felis mauris, molestie et iaculis eget,
                                            eleifend in sem. Integer eget imperdiet nisl. Curabitur interdum accumsan lectus, ut feugiat libero fringilla ac. Ut ut venenatis libero. Aliquam erat volutpat. Nunc ultricies velit ut libero cursus, ac tincidunt sem tristique.
                                            </div>
                                        </div></div><div class="scrollbar" style="width: 779px;"><div class="track" style="width: 779px;"><div class="thumb" style="left: 0px; width: 466.800769230769px;"><div class="end"></div></div></div></div></div>
                                    </div>
                                </div><!-- End .widget -->

                           </section>

    </div>

<script type="text/javascript">

    $('.bootbox-confirm-trigger').click(function(){

        var self = this;
        var name = $(this).attr("data-name");

        bootbox.confirm("Are you sure you want to delete " + name + "?", function(result) {

            if( result === true ){

                var delete_id = $(self).attr("data-id");

                var data = JSON.stringify({

                    "user_id": {{ Auth::user()->id }},
                    "delete_id": delete_id
                });

                $.ajax({
                    type: 'POST',
                    url: '/admin/users/delete',
                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                    dataType: 'json',
                    data: {'data': data}
                });

                $( "#user_" +  delete_id ).remove();

                var new_count = $( "#count" ).text() - 1;

                $( "#count" ).text(new_count);

            }
        });
    });

</script>

@stop