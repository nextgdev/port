@extends('layouts.master')

@section('header_javascript')

@stop

@section('content')

                <div class="window">
                    <div class="actionbar">
                        <div class="pull-left">
                            <ul class="ext-tabs">
                                <li class="active">
                                    <a href="#content-tab-1">Shortlist</a>
                                </li>
                            </ul><!-- End .ext-tabs -->
                        </div>
                        <div class="pull-right">
                        </div>
                    </div><!-- End .actionbar-->
                    <div class="tab-content">
                        <div id="content-tab-1" class="tab-pane active">
                            <div class="inner-padding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Company Name</th>
                                            <th scope="col">Ticker</th>
                                            <th scope="col">Exchange</th>
                                            <th scope="col">Current Price</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($companies as $company)
                                        <tr>
                                            <td>{{ $company->id }}</td>
                                            <td>{{ $company->company_name }}</td>
                                            <td>{{ $company->ticker }}</td>
                                            <td>{{ $company->exchange }}</td>
                                            <td>{{ $company->current_price }}</td>
                                            <td><a href="#">Edit</a> | <a href="#">Delete</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                        </div>
                </div>
@stop