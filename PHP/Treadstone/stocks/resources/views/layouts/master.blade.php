<?php $start_time = microtime(TRUE) ?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8 no-js">      <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9 no-js">           <![endif]-->
<!--[if gt IE 9]>  <html class="no-js">                       <![endif]-->
<!--[if !IE]><!--> <html class="no-js">                       <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>TreadStone</title>

    <!-- // Stylesheets // -->

    <link rel="stylesheet" href="/editor/bootstrap/core/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/editor/bootstrap/select2/select2.css"/>
    <link rel="stylesheet" href="/editor/bootstrap/datepicker/css/datepicker.css"/>
    <link rel="stylesheet" href="/editor/bootstrap/fileupload/bootstrap-fileupload.min.css"/>
    <link rel="stylesheet" href="/editor/bootstrap/typeahead/typeahead.min.css"/>
    <link rel="stylesheet" href="/editor/bootstrap/colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" href="/editor/bootstrap/timepicker/css/bootstrap-timepicker.min.css"/>
    <link rel="stylesheet" href="/editor/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/editor/css/bootstrap-custom.css"/>
    <link rel="stylesheet" href="/editor/css/bootstrap-extended.css"/>
    <link rel="stylesheet" href="/editor/css/animate.min.css"/>
    <link rel="stylesheet" href="/editor/css/helpers.css"/>
    <link rel="stylesheet" href="/editor/css/base.css"/>
    <link rel="stylesheet" href="/editor/css/dark-theme.css"/>
    <link rel="stylesheet" href="/editor/css/mediaqueries.css"/>

    <!-- // Helpers // -->
    <script src="/editor/js/libs/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/editor/js/plugins/modernizr.min.js"></script>
    <script src="/editor/js/plugins/mobiledevices.js"></script>

    <!-- // Bootstrap // -->

    <script src="/editor/bootstrap/core/dist/js/bootstrap.min.js"></script>
    <script src="/editor/bootstrap/select2/select2.min.js"></script>
    <script src="/editor/bootstrap/bootboxjs/bootboxjs.min.js"></script>
    <script src="/editor/bootstrap/holder/holder.min.js"></script>
    <script src="/editor/bootstrap/typeahead/typeahead.min.js"></script>
    <script src="/editor/bootstrap/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="/editor/bootstrap/fileupload/bootstrap-fileupload.min.js"></script>
    <script src="/editor/bootstrap/inputmask/bootstrap-inputmask.min.js"></script>
    <script src="/editor/bootstrap/colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="/editor/bootstrap/timepicker/js/bootstrap-timepicker.min.js"></script>

    <!-- // Custom/premium plugins // -->

    <script src="/editor/js/plugins/responsivetables.1.0.min.js"></script>
    <script src="/editor/js/plugins/responsivehelper.1.0.min.js"></script>
    <script src="/editor/js/plugins/mainmenu.1.0.min.js"></script>
    <script src="/editor/js/plugins/easyfiletree.1.0.min.js"></script>
    <script src="/editor/js/plugins/autosaveforms.1.0.min.js"></script>
    <script src="/editor/js/plugins/chainedinputs.1.0.min.js"></script>
    <script src="/editor/js/plugins/checkboxtoggle.1.0.min.js"></script>
    <script src="/editor/js/plugins/bootstraptabsextend.1.0.min.js"></script>
    <script src="/editor/js/plugins/lockscreen.1.0.min.js"></script>
    <script src="/editor/js/plugins/autoexpand.1.0.min.js"></script>
    <script src="/editor/js/plugins/notify.1.0.min.js"></script>
    <script src="/editor/js/plugins/powerwizard.1.0.min.js"></script>
    <script src="/editor/js/plugins/simpleselect.1.0.min.js"></script>
    <script src="/editor/js/plugins/tinycontextmenu.1.0.min.js"></script>

    <!-- // Third-party plugins // -->
    <script src="/editor/js/plugins/tinyscrollbar.min.js"></script>
    <script src="/editor/js/plugins/prism.min.js"></script>
    <script src="/editor/js/plugins/h5f.min.js"></script>
    <script src="/editor/js/plugins/fullcalendar.min.js"></script>
    <script src="/editor/js/plugins/hogan-2.0.0.js"></script>
    <script src="/editor/js/plugins/layout.min.js"></script>
    <script src="/editor/js/plugins/masonry.pkgd.min.js"></script>
    <script src="/editor/js/plugins/json2.js"></script>

    <!-- // jQuery // -->
    <script src="/editor/js/plugins/jquery.tablesorter.min.js"></script>
    <script src="/editor/js/plugins/jquery.tablesorter.widgets.min.js"></script>
    <script src="/editor/js/plugins/jquery.tablesorter.pager.min.js"></script>
    <script src="/editor/js/plugins/jquery.knob.js"></script>
    <script src="/editor/js/plugins/jquery.nouislider.min.js"></script>
    <script src="/editor/js/plugins/jquery.autosize-min.js"></script>
    <script src="/editor/js/plugins/jquery.magnific-popup.min.js"></script>
    <script src="/editor/js/plugins/jquery.pwstrength.min.js"></script>
    <script src="/editor/js/plugins/jquery.mixitup.min.js"></script>
    <script src="/editor/js/plugins/jquery.vticker.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <!-- // Custom //-->
    <script src="/editor/js/plugins/plugins.js"></script>
    <script src="/editor/js/plugins/demo.js"></script>
    <script src="/editor/js/plugins/main.js"></script>

    @yield('header_javascript')

    <script>
        <?php
        if(Auth::user()->is_admin < 1){

            $terminal = DB::table('terminal')
            ->where('is_admin', 0)
            ->get();
        }
        else{

            $terminal = DB::table('terminal')->get();
        }
        ?>

      $(function() {
        var availableTags = [

        @foreach ($terminal as $command)

            "{{ $command->command }}",

        @endforeach
        ];
        $( "#terminal_value" ).autocomplete({
          source: availableTags
        });

        $('ul.ui-autocomplete').css({
        color: '#BBB',
        background: '#333',
        border: 0
        });
      });
    </script>
</head>
<body>
    <div id="container" class="clearfix">
    <!-- ********************************************
         * SIDEBAR MAIN:                            *
         *                                          *
         * the part which contains the main         *
         * navigation, logo, search and more...     *
         ******************************************** -->

        <aside id="sidebar-main" class="sidebar">

            <div class="sidebar-logo">
                <a href="/dashboard" id="logo-big"><h1>TreadStone</h1></a>
            </div><!-- End .sidebar-logo -->

            <!-- ********** -->
            <!-- NEW MODULE -->
            <!-- ********** -->

            <div class="sidebar-module">
                <div class="sidebar-profile">
                    <img src="/editor/images/users/{{Auth::user()->profile_image}}" alt="" class="avatar"/>
                    <ul class="sidebar-profile-list">
                        <li><h3>Hi, {{ Auth::user()->name }}</h3></li>
                        <li><a href="/profile">Profile</a>  | <a href="/inbox">Mail</a> | <a href="{{ url('/auth/logout') }}">Logout</a></li>
                    </ul>
                </div><!-- End .sidebar-profile -->
            </div><!-- End .sidebar-module -->

            <div class="sidebar-line"><!-- A seperator line --></div>

            <!-- * Tabs can be removed, if so dont forget * -->
            <!-- * to remove the .tab-pane divs(wrapper). * -->

            <ul class="ext-tabs-sidebar">
                <li class="active">
                    <a href="#sidebar-tab-1"><i class="fa fa-bars"></i> Menu</a>
                </li>
                <li class="">
                    <a href="#sidebar-tab-2"><i class="fa fa-user"></i> Social</a>
                </li>
            </ul><!-- End .ext-tabs-sidebar -->
            <div class="tab-content">
                <div id="sidebar-tab-1" class="tab-pane active clearfix">

                    <!-- ********** -->
                    <!-- NEW MODULE -->
                    <!-- ********** -->

                    <div class="sidebar-module">
                        <nav class="sidebar-nav-v2">
                            <ul>
                                <li>
                                    <a href="/dashboard"><i class="fa fa-dashboard"></i> Dashboard </a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-table"></i> Stocks <i class="fa fa-caret-left pull-right"></i></a>

                                    <!-- * sub menu * -->
                                    <ul>
                                        @foreach ($companies as $company)
                                        <li style="clear: left;">
                                            <a href="/stock/{{ $company->ticker }}">
                                            <?php
                                            $cname = substr( $company->company_name, 0, 25 );

                                            echo $cname;

                                            if( strlen($company->company_name) >= 25 ){
                                                echo "..";
                                            }

                                            ?>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>

                                @if ( Auth::user()->is_admin == 1 )

                                <li>
                                    <a href="#">
                                        <i class="fa fa-eye"></i>
                                        <font color="red">Admin </font>
                                        <i class="fa fa-caret-left pull-right"></i>
                                    </a>

                                    <!-- * sub menu * -->
                                    <ul class="display: block;" style="display: block;">
                                    <li><a href="/admin/users">Users</a></li>
                                    <li><a href="/admin/companies">Companies</a></li>
                                    <li><a href="/admin/feeds">Feeds</a></li>
                                    <li><a href="/admin/planning">Planning</a></li>
                                    </ul>
                                </li>

                                @endif
                            </ul>
                        </nav><!-- End .sidebar-nav-v1 -->

                    </div><!-- End .sidebar-module -->
                </div>

                <div id="sidebar-tab-2" class="tab-pane clearfix">

                    <!-- ********** -->
                    <!-- NEW MODULE -->
                    <!-- ********** -->

                    <div class="sidebar-module">
                        <nav class="sidebar-nav-v2">
                            <ul>
                                <li class="page-arrow">

                                </li>
                            </ul>
                        </nav><!-- End .sidebar-nav-v1 -->

                    </div><!-- End .sidebar-module -->
                </div>

            <div class="sidebar-line"><!-- A seperator line --></div>

            <div class="sidebar-heading" data-module-toggle="false">
                <h3>
                    Stages of Development
                </h3>
            </div>

            <!-- ********** -->
            <!-- NEW MODULE -->
            <!-- ********** -->

            <div class="sidebar-module">
                @foreach ($plannings as $planning)

                    <?php
                        $total = App\Planning::where('stage', $planning->stage)
                        ->count();

                        $done = App\Planning::where('stage', $planning->stage)
                        ->where('status', 1)
                        ->count();
                        $progress = round((100 / $total) * $done, 0);
                    ?>

                <div class="progress-project">
                    <div class="progress-project-header">
                        <h5>Stage {{ $planning->stage }}</h5><span>{{$progress}}<span class="text-muted">/100</span></span>
                    </div>
                    <div class="progress bar-small">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{{$progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$progress}}%;">
                            <span class="sr-only">{{$progress}}% Complete</span>
                        </div>
                    </div>
                </div><!-- End .progress-project -->
                <div class="spacer-20"></div>
                @endforeach

            </div><!-- End .sidebar-module -->


            </div><!-- End .tab-content -->



        </aside><!-- End aside -->

        <div id="main" class="clearfix">

        <!-- ********************************************
             * MAIN HEADER:                             *
             *                                          *
             * the part which contains the breadcrumbs, *
             * dropdown menus, toggle sidebar button    *
             ******************************************** -->

            <header id="header-main">
                <div class="header-main-top">
                    <div class="pull-left">

                        <!-- * This is the responsive logo * -->

                        <a href="index.html" id="logo-small"><h4>Vanguard14</h4><h5>/editor</h5></a>
                    </div>
                    <div class="pull-right">

                        <!-- * This is the trigger that will show/hide the menu * -->
                        <!-- * if the layout is in responsive mode              * -->

                        <a href="#" id="responsive-menu-trigger">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div><!-- End #header-main-top -->
                <div class="header-main-bottom">
                    <div class="pull-left">
                            <a href="#" class="btn" id="INDUIND">

                            </a>
                            <a href="#" class="btn" id="SPXIND">

                            </a>
                            <a href="#" class="btn" id="UKXIND">

                            </a>
                            <a href="#" class="btn" id="NK1IND">

                            </a>
                            <a href="#" class="btn" id="EURUSDCUR">

                            </a>
                            <a href="#" class="btn" id="CL1COM">

                            </a>
                    </div>
                    <div class="pull-right">
                        <p>Version 1.0.0</p>
                    </div>
                </div><!-- End #header-main-bottom -->
            </header><!-- End #header-main -->

            <div id="content" class="clearfix">

            <!-- ********************************************
                 * HEADER SEC:                              *
                 *                                          *
                 * the part which contains the page title,  *
                 * buttons and dropdowns.                   *
                 ******************************************** -->

                <header id="header-sec">
                    <div class="inner-padding">
                        <div class="pull-left">
                            <h2>{{ $name }}</h2>
                        </div>
                        <div class="pull-right">
                            <div class="btn-group">
                                <a class="btn btn-default" href="#" id="modal-update-trigger">
                                Request Feature
                                </a>
                            </div><!-- End .btn-group -->
                         </div>
                    </div><!-- End .inner-padding -->
                </header><!-- End #header-sec -->

                <!-- ********************************************
                     * WINDOW:                                  *
                     *                                          *
                     * the part which contains the main content *
                     ******************************************** -->

                <div class="window">
                <div class="actionbar">
                        <div class="pull-left" style="margin-left:10px;">
                            <div class="ui-widget ">
                                <form role="form" id="form-login" method="POST" action="{{ url('/terminal') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input id="terminal_value" name="terminal_value" class="form-control" style="border: 0px; height:28px; margin-top:0px; width:600px;" placeholder="Terminal > ">
                                </form>
                            </div>
                        </div>
                    </div>

                    @yield('content')

                </div><!-- End .window -->

                <!-- ********************************************
                     * FOOTER MAIN:                             *
                     *                                          *
                     * the part which contains things like      *
                     * chat, buttons, copyright and             *
                     * dropup menu(s).                          *
                     ******************************************** -->

                <footer id="footer-main" class="footer-sticky">
                    <div class="footer-main-inner">
                        <div class="pull-left">
                            <p>Copyright © {{ date('Y') }} Treadstone -
                            <?php
                            $end_time = microtime(TRUE);
                            $time_taken = $end_time - $start_time;
                            $time_taken = round($time_taken,5);
                            ?>
                            {{ 'Page generated in '.$time_taken.' seconds.' }}
                            </p>
                        </div>
                        <div class="pull-right">
                            <div class="dropup">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-user"></i>
                                </a>
                                <div role="menu" class="dropdown-menu pull-right ext-dropdown-inbox">
                                    <div class="ext-dropdown-header">
                                        <h5>Users currently online:</h5>
                                        <span class="indicator-dot">{{ App\User::where('is_online', '>', 0)->count() }}</span>
                                    </div>
                                    <div class="ext-dropdown-inbox-content">

                                        <div>
                                            <ul>
                                            @foreach ($online_users as $user_online)

                                                @if ( $user_online->id == Auth::user()->id )
                                                    <li><span><strong>You:</strong>
                                                    </span> Online</li>
                                                @else
                                                    <li><span><strong>{{ $user_online->name }}:</strong>
                                                        </span> Online (<a href="#" id="private_chat" data-userid="{{ $user_online->id }}">Private Chat</a>)</li>
                                                @endif

                                            @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a href="#" class="btn" id="toggle-footer">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div><!-- End .footer-main-inner -->
                </footer><!-- End #footer-main -->
            </div><!-- End #content -->
        </div><!-- End #main -->
    </div><!-- End #container -->

    <!--Modal -->
    <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Request a new Feature</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-bottom:-20px">
                        <div class="col-xs-3">
                            <i class="fa fa-cog" style="font-size:120px;color:#ccc"></i>
                        </div>
                        <div class="col-xs-9">
                            <p>
                            If you want to see something new on the site that improves it and/or your user own user experience, please do submit the idea below:
                            </p>
                            <div class="spacer-20"></div>
                            <h4>New Feature:</h4>
                            <div class="spacer-20"></div>
                            <form>
                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom:20px;">
                                        <textarea class="form-control" id="ff-id-4"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary pull-right" id="submit_feature" data-dismiss="modal">Submit Request</button>
                </div>
            </div>
        </div>
     </div>

<script type="text/javascript">

$( document ).ready(function() {

        @yield('footer_javascript')

         $.e_notify.growl({
             title: 'Jeffrey has logged in!',
             text: 'Jeffrey has just logged in, click here to invite!',
             image: '/editor/images/users/avatar_3.jpg',
             position: 'left-bottom',
             delay: 0,
             time: 5000,
             speed: 500,
             effect: 'slidein',
             sticky: false,
             closable: false,
             maxOpen:1,
             className:'',
             onShow: function(){},
             onHide: function(){}
        });

    setInterval(function(){

        $.getJSON('/create_topbar', null, function(data) {

            $.each(data, function(i, item) {

                var id = "#" + item.bID.replace(':','');

                if(item.shortName == "EUR-USD"){
                    var html_g = "<strong>" + item.shortName + "</strong> <font color='white' id='numb'>$" + item.price +"</font>";
                    var html_r = "<strong>" + item.shortName + "</strong> <font color='white' id='numb'>$" + item.price +"</font>";
                }
                else{
                    var html_g = "<strong>" + item.shortName + "</strong> <font color='green' id='numb'>+" + item.priceChange1Day +"</font>";
                    var html_r = "<strong>" + item.shortName + "</strong> <font color='red' id='numb'>" + item.priceChange1Day +"</font>";
                }

                var check = item.priceChange1Day.toString();

                if( check.indexOf('-') >= 0){

                    $(id).html(html_r);
                }
                else{

                    $(id).html(html_g);
                }

            });
        });

        $.ajax({
            type: 'POST',
            url: '/user/is_online',
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
            dataType: 'json'
        });

    }, 1000);

    $('#submit_feature').click(function(){

        var feature = $('textarea#ff-id-4').val();
        var data = JSON.stringify({
            "feature": feature
        });

        $.ajax({
            type: 'POST',
            url: '/feature/add',
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
            dataType: 'json',
            data: {'data': data}
        });

        $('textarea#ff-id-4').val('');

    });

    $('#private_chat').click(function(){

        var invitee = $(this).attr("data-userid");

        var data = JSON.stringify({
            "user_id": {{ Auth::user()->id }},
            "invitee": invitee
        });

        $.ajax({
            type: 'POST',
            url: '/connect',
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
            dataType: 'json',
            data: {'data': data},
            success: function(ret){

                window.location.replace("/webrtc?ident=" + ret.identifier);

            },
            error: function(){
                alert('failure');
            }
        });

    });

});


</script>

</body>
</html>
