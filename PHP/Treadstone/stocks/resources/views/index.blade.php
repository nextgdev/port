@extends('layouts.master')

@section('content')

                    <div class="row ext-raster">
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="inner-padding">
                                        <div id="containergraph" style="max-width: 100%; height: 400px; padding: 0px; position: relative; "></div>

                                    </div><!-- End .inner-padding -->
                                </div>
                            </div><!-- End .row -->
                        </div>
                        <span class="breakpoint-sm ext-raster-line-8"></span>
                        <div class="col-sm-4">
                            <ul class="ext-tabs tabs-negative-position">
                                <li class="active">
                                    <a href="#content-tab-10">Planning</a>
                                </li>
                                <li>
                                    <a href="#content-tab-11">Twitter</a>
                                </li>
                                <li>
                                    <a href="#content-tab-12">
                                    <font color="red">FONU RSS</font>


                                    </a>

                                </li>
                                <li>
                                    <a href="#content-tab-13">Reddit Feed</a>
                                </li>
                            </ul><!-- End .ext-tabs -->
                            <div class="tab-content">
                                <div id="content-tab-10" class="tab-pane active">

                                    <div class="inner-padding">
                                        <div class="subheading" style="margin-bottom: 10px;">
                                            <h3>Planning Version 1</h3>
                                        </div>
                                        <div class="activity-stream">
                                        @foreach (App\Planning::distinct()->select('stage')->groupBy('stage')->get() as $planning)
                                        <strong>Stage {{ $planning->stage }}:</strong>
                                            <ul>
                                                @foreach (App\Planning::where('stage', $planning->stage)->get() as $task)

                                                    <li><font class="<?php if( $task->status === 1 ){ echo "stripe_text"; } ?>"
                                                        data-id="{{ $task->id }}">{{ $task->idea }}</font></li>

                                                @endforeach
                                            </ul>
                                        @endforeach
                                        </div><!-- End .activity-stream -->
                                    </div><!-- End .inner-padding -->
                                </div>

                                <div id="content-tab-11" class="tab-pane">
                                    <div class="inner-padding">

                                        <div class="row" style="margin-bottom: 20px;">

                                            @foreach ($tweets as $tweet)

                                                <div class="col-sm-12">
                                                    <div style="float:left; width: 150px;">
                                                        <strong>{{ $tweet->tweet_username }}: </strong>
                                                    </div>

                                                    <div style="float:left; width: 350px;">
                                                        <a href="https://twitter.com/{{ $tweet->tweet_username }}/status/{{ $tweet->tweet_id }}" target="_blank" id="">
                                                        {{ $tweet->tweet_text }}
                                                        </a>
                                                    </div>
                                                </div>

                                            @endforeach

                                        </div>
                                    </div><!-- End .inner-padding -->
                                </div>

                                <div id="content-tab-12" class="tab-pane">
                                    <div class="inner-padding">

                                        <div class="row" style="margin-bottom: 20px;">

                                            @foreach ($rss_feeds as $rss)

                                                <div class="col-sm-12">
                                                    <div style="float:left; width: 600px;"><a href="{{ $rss->url }}" target="_blank" id="{{ $rss->id }}">{{ $rss->title }}</a></div>
                                                </div>

                                            @endforeach

                                        </div>
                                    </div><!-- End .inner-padding -->
                                </div>

                                <div id="content-tab-13" class="tab-pane">
                                    <div class="inner-padding">

                                        <div class="subheading" style="margin-bottom: -0px; margin-top: -5px;" align="center">
                                           <img src="/editor/images/reddit-logo.png" width="45"></img>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px;" div="reddit">

                                            @foreach ($social as $reddit)

                                                <div class="col-sm-12">
                                                    <div style="width: 120px; float:left;"><font color="gray"> [/r/{{ $reddit->nice_name }}] </font></div>
                                                    <div style="float:left; width: 380px;"><a href="{{ $reddit->url }}" title="{{ $reddit->domain }} - {{ $reddit->ups }} upvotes" target="_blank" id="{{ $reddit->id }}">{{ $reddit->title }}</a></div>
                                                </div>

                                            @endforeach

                                        </div>
                                    </div><!-- End .inner-padding -->
                                </div>
                            </div>
                        </div>
                        <span class="ext-raster-line-bottom"></span>
                    </div>
                </div><!-- End .window -->

<script src="/Highstock-2.1.9/js/highstock.js"></script>
<script src="/Highstock-2.1.9/js/modules/exporting.js"></script>

<script type="text/javascript">

    $.getJSON('/charts/FONU.json', function (data) {

        // create the chart
        $('#containergraph').highcharts('StockChart', {


            title: {
                text: 'FONU stock price updated as much as possible'
            },

            subtitle: {
                text: 'Using explicit breaks for nights and weekends'
            },

            rangeSelector : {
                buttons : [{
                    type : 'hour',
                    count : 1,
                    text : '1h'
                }, {
                    type : 'day',
                    count : 2,
                    text : '1D'
                }, {
                    type : 'all',
                    count : 3,
                    text : 'All'
                }],
                selected : 1,
                inputEnabled : false
            },

            series : [{
                name : 'FONU',
                type: 'area',
                data : data,
                gapSize: 0,
                tooltip: {
                    valueDecimals: 5
                },
                fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                threshold: null
            }]
        });
    });
/**
 * Dark theme for Highcharts JS
 * @author  Torstein Honsi
 */

// Load the fonts
Highcharts.createElement('link', {
   href: '//fonts.googleapis.com/css?family=Unica+One',
   rel: 'stylesheet',
   type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);

Highcharts.theme = {
   colors: ["#E8443A", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
   chart: {
      backgroundColor: {
         linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
         stops: [
            [0, '#2a2a2b'],
            [1, '#3e3e40']
         ]
      },
      style: {
         fontFamily: "'Unica One', sans-serif"
      },
      plotBorderColor: '#606063'
   },
   title: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase',
         fontSize: '20px'
      }
   },
   subtitle: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase'
      }
   },
   xAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
   },
   yAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
   },
   tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
   },
   plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333'
         }
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
   },
   legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
   },
   credits: {
      style: {
         color: '#666'
      }
   },
   labels: {
      style: {
         color: '#707073'
      }
   },

   drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
   },

   navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
   },

   // scroll charts
   rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
               fill: '#707073',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            },
            select: {
               fill: '#000003',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
   },

   navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
   },

   scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
   },

   // special colors for some of the
   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);




        </script>
@stop