@extends('layouts.master')

@section('content')
<div class="inner-padding">
    <table class="table">
        <thead>
            <tr>
                <th scope="col" width="25">#</th>
                <th scope="col" width="600">Tweet</th>
                <th scope="col">Keywords</th>
                <th scope="col">Picture IDs</th>
                <th scope="col">Created At</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($tweets as $tweet)
            <tr>
                <td>{{ $tweet->id }}</td>
                <td>{{ $tweet->tweet_message }}</td>
                <td>dasdsad</td>
                <td>{{ $tweet->tweet_pictures_online }}</td>
                <td>{{ $tweet->created_at }}</td>
                <td><a data-id="{{ $tweet->id }}" href="#" id="tweet-edit">Edit</a> | 
                <a data-id="{{ $tweet->id }}" href="#">Delete</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

    <!--Modal -->
    <div class="modal fade" id="modal-tweet" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Tweet</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-bottom:-20px">
                    	<div class="inner-padding">
                    		<div class="row"> 
                                <div class="col-sm-3"> 
                                    <label>Tweet:</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea name="tweet_text" id="tweet_text" 
                                    class="form-control"></textarea>                             
                                </div>
                            </div>
                            <div class="spacer-10"></div>
                    		<div class="row"> 
                                <div class="col-sm-3"> 
                                    <label>Project:</label>
                                </div>
                                <div class="col-sm-9">
                                    <select id="tweet_cat" class="form-control">
                                    </select>                             
                                </div>
                            </div>
                            <div class="spacer-10"></div>
                    		<div class="row"> 
                                <div class="col-sm-3"> 
                                    <label>Image IDs:</label>
                                </div>
                                <div class="col-sm-9">
									<input id="tweet_image" type="text" 
									class="form-control">                            
                                </div>
                            </div>
                            <div class="spacer-10"></div>
                    		<div class="row"> 
                                <div class="col-sm-3"> 
                                    <label>Keywords:</label>
                                </div>
                                <div class="col-sm-9">
									<input type="text" id="tweet_keywords" 
									class="form-control">                            
                                </div>
                            </div>
                    	</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary pull-right" id="change_tweet" data-dismiss="modal">Edit Tweet</button>
                </div>
            </div>
        </div>
     </div>
@stop

@section('footer_javascript')
	
	$('#tweet-edit').click(function(e){
		$('#modal-tweet').modal();
        
		id = $(this).attr('data-id');

        $.getJSON( '/twitter/json/tweet/' + id, function( data ) {

        	$("#tweet_text").val(data.tweet_message);
        	$("#tweet_image").val(data.tweet_pictures_online);
        	$("#tweet_keywords").val(data.tweet_keywords);

        	project_id = data.tweet_project;

        });


        $.getJSON( '/twitter/json/categories', function( cats ) {
        	
        	$.each(cats, function( index, value ) {
  				
  				if (project_id === value.id){
  					
  					$( "#tweet_cat" ).append( "<option value='"+ value.id +"' selected>"+
  					value.tweet_project_name +"</option>" );
  				}
  				else{
					$( "#tweet_cat" ).append( "<option value='"+ value.id +"'>"+
  					value.tweet_project_name +"</option>" );
  				}


			});
        });


        e.preventDefault();

	});

    $('#change_tweet').click(function(e){
        
        var data = JSON.stringify({
            "tweet_text": $("#tweet_text").val(),
            "tweet_image": $("#tweet_image").val(),
            "tweet_keywords": $("#tweet_keywords").val(),
            "tweet_category": $("#tweet_cat").val()
        });

        $.ajax({
            type: 'POST',
            url: '/twitter/update',
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
            dataType: 'json',
            data: {'data': data}
        });

        location.reload();

    });
@stop