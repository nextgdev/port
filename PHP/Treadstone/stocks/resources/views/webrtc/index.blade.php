@extends('layouts.master')
<?php
    $identifier = $_GET['ident'];
?>

@section('header_javascript')
    <script src="https://simplewebrtc.com/latest-v2.js"></script>

    <script type="text/javascript">
    var webrtc = new SimpleWebRTC({
      // the id/element dom element that will hold "our" video
      localVideoEl: 'localVideo',
      // the id/element dom element that will hold remote videos
      remoteVideosEl: '',
      // immediately ask for camera access
      autoRequestMedia: true,
      url: 'http://178.62.18.175:5000'
    });

    // we have to wait until it's ready
    webrtc.on('readyToCall', function () {
      // you can name it anything
      webrtc.joinRoom('{{ $identifier }}');
    });

// a peer video has been added
webrtc.on('videoAdded', function (video, peer) {
    console.log('video added', peer);
    var remotes = document.getElementById('remotes');
    if (remotes) {
        var container = document.createElement('div');
        container.className = 'videoContainer';
        container.id = 'container_' + webrtc.getDomId(peer);
        container.appendChild(video);

        // suppress contextmenu
        video.oncontextmenu = function () { return false; };

        remotes.appendChild(container);
    }

    // show the ice connection state
    if (peer && peer.pc) {
        var connstate = document.createElement('div');
        connstate.className = 'connectionstate';
        container.appendChild(connstate);
        peer.pc.on('iceConnectionStateChange', function (event) {
            switch (peer.pc.iceConnectionState) {
            case 'checking':
                connstate.innerText = 'Connecting to peer...';
                break;
            case 'connected':
            case 'completed': // on caller side
                connstate.innerText = 'Connection established.';
                break;
            case 'disconnected':
                connstate.innerText = 'Disconnected.';
                break;
            case 'failed':
                break;
            case 'closed':
                connstate.innerText = 'Connection closed.';
                break;
            }
        });
    }

    // show the remote volume
    var vol = document.createElement('meter');
    vol.id = 'volume_' + peer.id;
    vol.className = 'volume';
    vol.min = -45;
    vol.max = -20;
    vol.low = -40;
    vol.high = -25;
    container.appendChild(vol);
});

// local volume has changed
webrtc.on('volumeChange', function (volume, treshold) {
    showVolume(document.getElementById('localVolume'), volume);
});

function showVolume(el, volume) {
    console.log('showVolume', volume, el);
    if (!el) return;
    if (volume < -45) volume = -45; // -45 to -20 is
    if (volume > -20) volume = -20; // a good range
    el.value = volume;
}

// a peer video was removed
webrtc.on('videoRemoved', function (video, peer) {
    console.log('video removed ', peer);
    var remotes = document.getElementById('remotes');
    var el = document.getElementById(peer ? 'container_' + webrtc.getDomId(peer) : 'localScreenContainer');
    if (remotes && el) {
        remotes.removeChild(el);
    }
});



    </script>

<style>
.message_display{

   margin-left: 5px;
   margin-right: 5px;
   line-height: 20px;
   text-align: left;
}

.message_display_alt{

   margin-left: 5px;
   margin-right: 5px;
   line-height: 20px;
   text-align: left;
   background-color: #fff;
}

.message_user{

    width: 100px;
    float:left;
}

.message_msg{

    float:left;
    width:950px;
}

.videoContainer {
    position: relative;
    width: 400px;
    height: 300px;
    background-color: #000;
}
.videoContainer video {
    position: absolute;
    top: 0;
    left: 0;
    width: 400px;
    height: 300px;
}

.connectionstate {
    position: absolute;
    top: 0px;
    width: 100%;
    text-align: center;
    color: #fff
}

.volume {
    position: absolute;
    left: 15%;
    width: 70%;
    bottom: 2px;
    height: 10px;
}
</style>

@stop

@section('content')

<div class="inner-padding">

    <div class="row demo-grid">

        <div class="col-sm-4" style="position: relative; min-height: 150px; height:600px; width:400px;">

            <div id="remotes"></div>


            <video id="localVideo" height="300" width="400" style="position: absolute; bottom: 0; left: 0; background-color: #000; height:300px;"></video>
        </div>

        <div class="col-sm-8" style="position: relative; min-height: 150px; height:600px;">

        <div style="overflow-y: auto; position: relative; min-height: 150px; height:568px; padding-bottom: 3px;" id="chatbox">

            <?php
            $chats = DB::table('chat')
            ->where('identifier', $identifier)
            ->orderBy('id', 'asc')
            ->get();

            $chat_info = DB::table('chat')
            ->where('identifier', $identifier)
            ->first();
            ?>



            @foreach ($chats as $chat)

            <div class="message_display" id="{{ $chat->id }}">

                <div class="message_user">
                    <font color="#808080"><strong>{{ App\User::find($chat->user_id)->name }}</font>: </strong>
                </div>

                <div class="message_msg">
                    <font color="#f0f0f5">{{{ $chat->message }}}</font>
                </div>

            </div>

            @endforeach

        </div>

            <div id="chat_input" style="position: absolute; bottom: 0; left: 0; width:100%; border-color: #000; border:1px; border-style: solid;" class="">

                <input type="text" id="enter" autofocus="" class="form-control" >

            </div>

        </div>
    </div>

</div>

<script type="text/javascript">

$("#chatbox").animate({ scrollTop: $("#chatbox")[0].scrollHeight}, 400);

$('#enter').keydown(function (event) {

    var keypressed = event.keyCode || event.which;

    if (keypressed == 13 || keypressed == 10) {
        event.preventDefault();

        if( $(this).val() ){

            var message = $(this).val();

            var data = JSON.stringify({
                "user_id": {{ Auth::user()->id }},
                "receiver_id": {{ $chat_info->receiver_id }},
                "message": message,
                "identifier": '{{ $identifier }}'
                });

            $.ajax({
                type: 'POST',
                url: '/encrypted_chat',
                headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                dataType: 'json',
                data: {'data': data}
            });

            $(this).val('');
        }

    }

});

setInterval(function(){

    var last_id = $("div.message_display:last").attr('id');

    $.getJSON( "/encrypted_chat", { id: "<?php echo $identifier; ?>" })

        .done(function( data ) {

        function yol(input){

            if( input.indexOf("http") > -1 || input.indexOf("https") > -1 ){

                return input;
            }
            else{

                return escape(input);
            }

        }

        if( data.id > last_id ){

            $("#chatbox").append($("<div>", { id: data.id, class: "message_display" }).html('<div class="message_user"><font color="#808080"><strong>'+data.username+'</font>: </strong></div><div class="message_msg"><font color="#f0f0f5">'+yol(data.message)+'</font></div>'));

            $("#chatbox").animate({ scrollTop: $("#chatbox")[0].scrollHeight}, 400);
        }



    });

}, 500);
</script>

@stop