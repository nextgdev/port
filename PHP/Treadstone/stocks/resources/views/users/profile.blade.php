@extends('layouts.master')

@section('content')
<div class="inner-padding">

@if ( isset($message) )
<div class="alert alert-block alert-inline-top alert-dismissable">

    <button type="button" class="close" data-dismiss="alert">×</button>
    <h4>Message:</h4>
    {{ $message }}

</div>
@endif

@if(Session::has('success'))
<div class="alert alert-block alert-inline-top alert-dismissable">

    <button type="button" class="close" data-dismiss="alert">×</button>
    <h4>Message:</h4>
    {!! Session::get('success') !!}

</div>
@endif


                                <div class="subheading">
                                    <h3>Edit Profile</h3>
                                    <p>On this page you can change your profile settings and other preferences.</p>
                                </div>
                                <form class="form-horizontal" role="form" id="edit-profile" method="POST" action="{{ url('/profile') }}">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Name</label>
                                    </div>
                                    <div class="col-sm-9">

                                            <input required class="form-control" type="text" placeholder="Name" value="{{ Auth::user()->name }}" name="name">

                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>E-Mail</label>
                                    </div>
                                    <div class="col-sm-9">

                                            <input required class="form-control" type="text" placeholder="Your E-mail" value="{{ Auth::user()->email }}" name="email">

                                        <div class="helper-text-box">
                                            <div class="form-helper-header">Remember:</div>
                                            <p>
                                            Your E-Mail address is also your login, so don't mess up!
                                            </p>
                                        </div><!-- End .helper-text-box -->
                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Password</label>
                                    </div>
                                    <div class="col-sm-9">

                                        <input required class="form-control" type="password" placeholder="Your password" name="password">

                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Password Again</label>
                                    </div>
                                    <div class="col-sm-9">

                                            <input class="form-control" type="password" placeholder="Your password again" name="password_again">

                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">

                                    </div>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        <input type="submit" class="btn btn-primary" name="save_personal" value="Update User Information">
                                    </div>
                                </div>
                                </form>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">

                                    </div>
                                    <div class="col-sm-9">
                                        <label>
                                            <font color="#3071A9">Customizing Dashboard</font>
                                        </label>
                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <form class="form-horizontal" role="form" id="edit-profile" method="POST" action="{{ url('/profile') }}">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>RSS Feeds</label>
                                    </div>
                                    <div class="col-sm-9">
                                            <textarea class="form-control" placeholder="For example: http://www.website.com/feed/," name="feeds">{{ Auth::user()->feeds }}</textarea>

                                        <div class="helper-text-box">
                                            <div class="form-helper-header">Warning:</div>
                                            <p>
                                            If you want to add new RSS feeds, please add their urls and seperated by a comma.
                                            </p>
                                        </div><!-- End .helper-text-box -->
                                    </div>
                                </div>
                                <div class="spacer-25"></div>
                                <div class="row">
                                    <div class="col-sm-3">

                                    </div>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        <input type="submit" class="btn btn-primary" name="save_custom" value="Save Preferences">
                                    </div>
                                </div>
                                </form>
                                <div class="spacer-25"></div>
                                {!! Form::open(array('action' => 'UserController@upload_profile_picture','method'=>'POST', 'files'=>true)) !!}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Upload Profile Picture</label>
                                    </div>

                                    <div class="col-sm-9">

                                        <div class="fileupload fileupload-new" data-provides="fileupload">

                                            <div class="input-group">

                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>

                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-new">Select file</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    {!! Form::file('profile_picture') !!}
                                                </span>

                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>

                                        <div class="helper-text-box">
                                            <div class="form-helper-header">File upload</div>
                                                <p>
                                                    This plugin allows you to upload files just like a file input. The example above is build with a grid layout.
                                                </p>
                                        </div><!-- End .helper-text-box -->
                                        {!! Form::submit('Update Picture', array('class'=>'btn btn-primary')) !!}

                                        @if( Session::has('error') )
                                            <p class="errors">{!! Session::get('error') !!}</p>
                                        @endif
                                    </div>

                                </div>
                                {!! Form::close() !!}
                                <div class="spacer-40"></div>

@stop