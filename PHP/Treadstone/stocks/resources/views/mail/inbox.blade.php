@extends('layouts.master')

@section('content')

<div class="inner-padding">
                        <div class="cmanager">
                            <div class="cmanager-inner clearfix">
                                <div class="cmanager-header responsive-helper clearfix">
                                    <div class="pull-left">
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-default"><i class="fa fa-trash-o"></i></a>
                                            <a href="#" class="btn btn-default"><i class="fa fa-check"></i></a>
                                            <a href="#" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="dropdown">
                                            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-cog"></i>
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li>
                                                    <a href="#"><i class="fa fa-pencil"></i> Rename</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-plus"></i> Create archive</a>
                                                </li>
                                                <li class="disabled">
                                                    <a href="#"><i class="fa fa-magnet"></i> Duplicate</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-file"></i> New Files</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <a href="#" class="btn btn-default">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-default"><i class="fa fa-caret-left"></i></a>
                                            <a href="#" class="btn btn-default"><i class="fa fa-caret-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <form class="input-group width-200">
                                            <input type="text" name="" class="form-control" placeholder="Search here...">
                                            <div class="input-group-btn">
                                                <a href="#" class="clear-input"><i class="fa fa-times-circle"></i></a>
                                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="cmanager-content clearfix">
                                    <div class="cmanager-sidebar">
                                        <div class="inner-padding">
                                            <div class="filter-box">
                                                <label><input type="checkbox" checked=""><span></span> New</label>
                                                <label><input type="checkbox"><span></span> Read</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cmanager-subheader">
                                        <div class="pull-left">
                                            <a href="#" class="btn" id="cmanager-sidebar-trigger">
                                                <i class="fa fa-caret-left"></i>
                                            </a>
                                            <p>32 New tickets / 3 Open tickets</p>
                                        </div>
                                    </div>
                                    <div class="cmanager-window">
                                        <div>
                                        <table class="table" id="ticket-table" data-rt-breakpoint="600">
                                            <thead>
                                                <tr>
                                                    <th scope="col" class="th-square">
                                                        <label><input type="checkbox" class="checkbox-master"><span></span></label>
                                                    </th>
                                                    <th scope="col" class="th-status-label"></th>
                                                    <th scope="col">Subject</th>
                                                    <th scope="col" class="th-date">Date</th>
                                                    <th scope="col" class="th-agent">From</th>
                                                    <th scope="col" class="th-details"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($mails as $mail)
                                                <tr>
                                                    <td class="rt-hide-td">
                                                        <label><input type="checkbox"><span></span></label>
                                                    </td>
                                                    <td class="td-ticket rt-hide-td">
                                                        <span class="label label-danger label-ticket">Important</span>
                                                    </td>
                                                    <td class="rt-hide-td">
                                                        <strong>{{ $mail->message }}</strong>
                                                    </td>
                                                    <td class="rt-hide-td">{{ $mail->created_at }}</td>
                                                    <td class="rt-hide-td">{{ App\User::find($mail->user_id)->name }}</td>
                                                    <td class="td-btn rt-hide-td">
                                                        <a href="#" class="btn btn-default btn-sm table-toggle-tr">Open</a>
                                                    </td>
                                                </tr>

                                                <tr class="table-collapsible rt-exclude">
                                                    {{ $mail->message }}
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End .cmanager -->
                    </div>
@stop