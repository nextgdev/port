@extends('layouts.master')

@section('content')

                <div class="window">
                    <div class="actionbar">
                        <div class="pull-left">
                            <ul class="ext-tabs">
                                <li class="active">
                                    <a href="#content-tab-1">Summary</a>
                                </li>
                                <li class="">
                                    <a href="#content-tab-2">Balance Sheet</a>
                                </li>
                                <li class="">
                                    <a href="#content-tab-4">Income Statements</a>
                                </li>
                                <li class="">
                                    <a href="#content-tab-6">Cashflow Statements</a>
                                </li>
                            </ul><!-- End .ext-tabs -->
                        </div>
                        <div class="pull-right">
                        </div>
                    </div><!-- End .actionbar-->

                    <div class="tab-content">
                        <div id="content-tab-1" class="tab-pane active">
                            <div class="inner-padding">

                            <h1>
                                {{$company->company_name }}
                                <font color="orange">
                                @if (strpos($company->current_price, ".") !== false)
                                    ${{ $company->current_price }}
                                @else
                                    ${{ $company->current_price }}.00
                                @endif

                                </font>

                                @if($company->price_up < 0)
                                <font color="red">
                                <i class="fa fa-arrow-down" style="margin-right:20px;"></i>
                                @if (strpos($company->price_up, ".") !== false)
                                    ${{ $company->price_up }}0
                                @else
                                    ${{ $company->price_up }}.00
                                @endif

                                ({{ $company->price_percentage_up }}%)
                                </font>
                                @else
                                <font color="green">
                                <i class="fa fa-arrow-up" style="margin-right:20px;"></i>
                                @if (strpos($company->price_up, ".") !== false)
                                    ${{ $company->price_up }}0
                                @else
                                    ${{ $company->price_up }}.00
                                @endif

                                ({{ $company->price_percentage_up }}%)
                                </font>
                                @endif
                            </h1>
                                <div class="spacer-20"></div>
                                <div class="faq">
                                    <article class="faq-group faq-open">
                                        <header>
                                            <i class="fa fa-minus"></i>
                                            <h3>Company Description</h3>
                                        </header>

                                        <div class="faq-content clearfix">
                                            <p>{{ $company->company_summary }}</p>
                                        </div>
                                    </article><!-- End .faq-group -->
                                </div>

                            <div class="spacer-20"></div>
                            </div>
                        </div>

                        <div id="content-tab-2" class="tab-pane">
                            <div class="inner-padding">

                                <div align="center">
                                    <div class="ext-tabs" style="border: none;">
                                    <a href="#content-tab-2">Annual Data</a>/<a href="#content-tab-3">Quarterly Data</a>
                                    </div>
                                </div>

                                <table width="100%" cellpadding="2" cellspacing="0" border="0" id="yui_3_9_1_8_1451393070700_39">
                                   <tbody id="yui_3_9_1_8_1451393070700_38">

                                      <tr>
                                         <td class="yfnc_modtitle1" colspan="2"><span class="yfi-module-title"><strong>Period Ending</strong></span></td>
                                        <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('period_ending');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td class="yfnc_modtitle1" align="right"><b>{{ $financial }}</b></td>
                                         @endforeach
                                      </tr>
                                      <tr>
                                         <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                      </tr>
                                      <tr>
                                         <td class="yfnc_d" colspan="6">
                                            <font color="orange"><strong>Assets</strong></font>
                                         </td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="6">Current Assets</td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Cash And Cash Equivalents</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_current_assets_candcequiv');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach

                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Short Term Investments</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_current_assets_shorttinvest');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Net Receivables</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_current_assets_netreceiv');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Inventory</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_current_assets_inventory');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Other Current Assets</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_current_assets_othercurrassets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Current Assets
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_current_assets_total');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <div class="spacer-10"></div>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Long Term Investments</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_long_term_investments');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Property Plant and Equipment</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_propertyplantequip');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Goodwill</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_goodwill');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Intangible Assets</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_intang_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Accumulated Amortization</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_accum_amortization');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Other Assets</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_other_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Deferred Long Term Asset Charges</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_def_longterm_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Assets
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('assets_total_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr>
                                         <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                      </tr>
                                      <tr>
                                         <td class="yfnc_d" colspan="6">
                                            <font color="orange"><strong>Liabilities</strong></font>
                                         </td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="6">Current Liabilities</td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Accounts Payable</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_curr_liabilities_acc_pay');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Short/Current Long Term Debt</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_curr_short_long_t_debt');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Other Current Liabilities</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_curr_other_curr_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Current Liabilities
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_total_curr_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Long Term Debt</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_long_term_debt');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Other Liabilities</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_other_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Deferred Long Term Liability Charges</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_deff_long_term_liability');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Minority Interest</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_minority_interest');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Negative Goodwill</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_negative_goodwill');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Liabilities
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('liabilities_total_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr>
                                         <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                      </tr>
                                      <tr>
                                         <td class="yfnc_d" colspan="6">
                                            <font color="orange"><strong>Stockholders' Equity</strong></font>
                                         </td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Misc Stocks Options Warrants</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_misc_stocks_options_warrants');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Redeemable Preferred Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_redeemable_pref_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Preferred Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_pref_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Common Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_comm_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Retained Earnings</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_ret_earnings');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Treasury Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_treasury_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Capital Surplus</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_capital_surplus');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Other Stockholder Equity</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_other_stockholder_equity');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Stockholder Equity
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('stockhequity_total_stockholder_equity');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#006600">
                                         <td colspan="2">
                                            <strong>
                                            Net Tangible Assets
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('net_tangible_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                   </tbody>
                                </table>

                                <div class="spacer-20"></div>
                            </div>
                        </div>

                        <div id="content-tab-3" class="tab-pane">

                            <div class="inner-padding">

                                <div align="center">
                                    <div class="ext-tabs" style="border: none;">
                                    <a href="#content-tab-2">Annual Data</a>/<a href="#content-tab-3">Quarterly Data</a>
                                    </div>
                                </div>

                                <table width="100%" cellpadding="2" cellspacing="0" border="0" id="yui_3_9_1_8_1451393070700_39">
                                   <tbody id="yui_3_9_1_8_1451393070700_38">

                                      <tr>
                                         <td class="yfnc_modtitle1" colspan="2"><span class="yfi-module-title"><strong>Period Ending</strong></span></td>
                                        <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('period_ending');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td class="yfnc_modtitle1" align="right"><b>{{ $financial }}</b></td>
                                         @endforeach
                                      </tr>
                                      <tr>
                                         <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                      </tr>
                                      <tr>
                                         <td class="yfnc_d" colspan="6">
                                            <font color="orange"><strong>Assets</strong></font>
                                         </td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="6">Current Assets</td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Cash And Cash Equivalents</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_current_assets_candcequiv');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach

                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Short Term Investments</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_current_assets_shorttinvest');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Net Receivables</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_current_assets_netreceiv');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Inventory</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_current_assets_inventory');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Other Current Assets</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_current_assets_othercurrassets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Current Assets
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_current_assets_total');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <div class="spacer-10"></div>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Long Term Investments</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_long_term_investments');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Property Plant and Equipment</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_propertyplantequip');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Goodwill</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_goodwill');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Intangible Assets</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_intang_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Accumulated Amortization</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_accum_amortization');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Other Assets</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_other_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Deferred Long Term Asset Charges</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_def_longterm_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Assets
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('assets_total_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr>
                                         <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                      </tr>
                                      <tr>
                                         <td class="yfnc_d" colspan="6">
                                            <font color="orange"><strong>Liabilities</strong></font>
                                         </td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="6">Current Liabilities</td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Accounts Payable</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_curr_liabilities_acc_pay');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Short/Current Long Term Debt</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_curr_short_long_t_debt');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td width="30" class="yfnc_tabledata1">
                                            <spacer type="block" width="30" height="1"></spacer>
                                         </td>
                                         <td>Other Current Liabilities</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_curr_other_curr_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Current Liabilities
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_total_curr_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Long Term Debt</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_long_term_debt');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Other Liabilities</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_other_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Deferred Long Term Liability Charges</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_deff_long_term_liability');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Minority Interest</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_minority_interest');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Negative Goodwill</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_negative_goodwill');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Liabilities
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('liabilities_total_liabilities');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr>
                                         <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                      </tr>
                                      <tr>
                                         <td class="yfnc_d" colspan="6">
                                            <font color="orange"><strong>Stockholders' Equity</strong></font>
                                         </td>
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Misc Stocks Options Warrants</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_misc_stocks_options_warrants');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Redeemable Preferred Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_redeemable_pref_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Preferred Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_pref_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Common Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_comm_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Retained Earnings</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_ret_earnings');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Treasury Stock</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_treasury_stock');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Capital Surplus</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_capital_surplus');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#333333">
                                         <td colspan="2">Other Stockholder Equity</td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_other_stockholder_equity');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right">{{ number_format($financial)}}&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#404040">
                                         <td colspan="2">
                                            <strong>
                                            Total Stockholder Equity
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('stockhequity_total_stockholder_equity');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                      <tr bgcolor="#bfbfbf">
                                         <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                      </tr>
                                      <tr bgcolor="#006600">
                                         <td colspan="2">
                                            <strong>
                                            Net Tangible Assets
                                            </strong>
                                         </td>
                                         <?php
                                         $financials = DB::table('Financials_BalanceSheet')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('net_tangible_assets');
                                         ?>

                                         @foreach ($financials as $financial)
                                         <td align="right"><strong>{{ number_format($financial)}}</strong>&nbsp;&nbsp;</td>
                                         @endforeach
                                      </tr>
                                   </tbody>
                                </table>

                                <div class="spacer-20"></div>

                            </div>

                        </div>


                        <div id="content-tab-4" class="tab-pane">

                            <div class="inner-padding">
                                <div align="center">
                                    <div class="ext-tabs" style="border: none;">
                                    <a href="#content-tab-4">Annual Data</a>/<a href="#content-tab-5">Quarterly Data</a>
                                    </div>
                                </div>
                            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                               <tbody>
                                  <tr class="yfnc_modtitle1" style="border-top:none;">
                                     <td colspan="2" style="">
                                        <span class="yfi-module-title"><strong>Period Ending</strong></span>
                                    </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('period_ending');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <th scope="col" style="text-align:right; font-weight:bold">{{ $income }}</th>
                                         @endforeach

                                  </tr>
                                  <br/>
                                  <tr>
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Total Revenue
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('total_revenue');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Cost of Revenue</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('cost_of_revenue');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr >
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Gross Profit
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('gross_profit');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td>
                                        <spacer type="block" height="1" width="1"></spacer>
                                     </td>
                                     <td class="yfnc_d" colspan="6">Operating Expenses</td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Research Development</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('research_development');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Selling General and Administrative</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('selling_general_and_administrative');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Non Recurring</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('non_recurring');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Others</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('others');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0; padding:0; " class="yfnc_d"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Total Operating Expenses</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('total_operating_expenses');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>

                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr>
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Operating Income or Loss
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('operating_income_or_loss');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td>
                                        <spacer type="block" height="1" width="1"></spacer>
                                     </td>
                                     <td class="yfnc_d" colspan="6">Income from Continuing Operations</td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Total Other Income/Expenses Net</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('total_other_income_expenses_net');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Earnings Before Interest And Taxes</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('earnings_before_interest_and_taxes');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Interest Expense</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('interest_expense');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Income Before Tax</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('income_before_tax');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Income Tax Expense</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('income_tax_expense');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Minority Interest</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('minority_interest');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">

                                     <td colspan="6" style="height:0; padding:0; " class="yfnc_d"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Net Income From Continuing Ops</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('net_income_from_cont_ops');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td>
                                        <spacer type="block" height="1" width="1"></spacer>
                                     </td>
                                     <td class="yfnc_d" colspan="6">Non-recurring Events</td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Discontinued Operations</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('disc_ops');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Extraordinary Items</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('extraordinary_items');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Effect Of Accounting Changes</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('effect_of_accounting_changes');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Other Items</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('other_items');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Net Income
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('net_income');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="2">Preferred Stock And Other Adjustments</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('pref_stock_and_other_adjustments');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:1px;padding:0; border-top:1px solid #bfbfbf;"></td>
                                  </tr>
                                  <tr bgcolor="#006600">
                                     <td colspan="2">
                                        <strong>
                                        Net Income Applicable To Common Shares
                                        </strong>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('net_income_applicable_to_common_shares');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                               </tbody>
                            </table>
                            <div class="spacer-20"></div>
                        </div>

                        </div>


                        <div id="content-tab-5" class="tab-pane">

                            <div class="inner-padding">
                                <div align="center">
                                    <div class="ext-tabs" style="border: none;">
                                    <a href="#content-tab-4">Annual Data</a>/<a href="#content-tab-5">Quarterly Data</a>
                                    </div>
                                </div>
                            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                               <tbody>
                                  <tr class="yfnc_modtitle1" style="border-top:none;">
                                     <td colspan="2" style="">
                                        <span class="yfi-module-title"><strong>Period Ending</strong></span>
                                    </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('period_ending');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <th scope="col" style="text-align:right; font-weight:bold">{{ $income }}</th>
                                         @endforeach

                                  </tr>
                                  <br/>
                                  <tr>
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Total Revenue
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('total_revenue');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Cost of Revenue</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('cost_of_revenue');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr >
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Gross Profit
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('gross_profit');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td>
                                        <spacer type="block" height="1" width="1"></spacer>
                                     </td>
                                     <td class="yfnc_d" colspan="6">Operating Expenses</td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Research Development</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('research_development');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Selling General and Administrative</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('selling_general_and_administrative');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Non Recurring</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('non_recurring');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Others</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('others');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0; padding:0; " class="yfnc_d"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Total Operating Expenses</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('total_operating_expenses');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>

                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr>
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Operating Income or Loss
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('operating_income_or_loss');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td>
                                        <spacer type="block" height="1" width="1"></spacer>
                                     </td>
                                     <td class="yfnc_d" colspan="6">Income from Continuing Operations</td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Total Other Income/Expenses Net</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('total_other_income_expenses_net');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Earnings Before Interest And Taxes</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('earnings_before_interest_and_taxes');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Interest Expense</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('interest_expense');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Income Before Tax</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('income_before_tax');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Income Tax Expense</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('income_tax_expense');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Minority Interest</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('minority_interest');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">

                                     <td colspan="6" style="height:0; padding:0; " class="yfnc_d"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Net Income From Continuing Ops</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('net_income_from_cont_ops');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td>
                                        <spacer type="block" height="1" width="1"></spacer>
                                     </td>
                                     <td class="yfnc_d" colspan="6">Non-recurring Events</td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Discontinued Operations</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('disc_ops');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Extraordinary Items</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('extraordinary_items');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Effect Of Accounting Changes</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('effect_of_accounting_changes');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td class="yfnc_tabledata1" width="30">
                                        <spacer type="block" width="30" height="1"></spacer>
                                     </td>
                                     <td>Other Items</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('other_items');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">
                                        <font color="orange">
                                        <strong>
                                        Net Income
                                        </strong>
                                        </font>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('net_income');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="2">Preferred Stock And Other Adjustments</td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('pref_stock_and_other_adjustments');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($income) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:1px;padding:0; border-top:1px solid #bfbfbf;"></td>
                                  </tr>
                                  <tr bgcolor="#006600">
                                     <td colspan="2">
                                        <strong>
                                        Net Income Applicable To Common Shares
                                        </strong>
                                     </td>
                                        <?php
                                         $incomes = DB::table('Financials_IncomeStatement')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('net_income_applicable_to_common_shares');
                                         ?>

                                         @foreach ($incomes as $income)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($income) }}</strong></td>
                                         @endforeach
                                  </tr>
                               </tbody>
                            </table>
                            <div class="spacer-20"></div>
                        </div>

                        </div>




                        <div id="content-tab-6" class="tab-pane">
                                <div align="center">
                                    <div class="ext-tabs" style="border: none;">
                                    <a href="#content-tab-6">Annual Data</a>/<a href="#content-tab-7">Quarterly Data</a>
                                    </div>
                                </div>

                            <div class="inner-padding">
                            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                               <tbody>
                                  <tr>
                                     <td class="yfnc_modtitle1" colspan="2">
                                     <span class="yfi-module-title"><strong>Period Ending</strong></span></td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('period_ending');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><b>{{ $cashflow }}</b></td>
                                         @endforeach
                                  </tr>
                                  <br/>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">

                                        <font color="orange"><strong>Net Income</strong></font>

                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('net_income');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><b>{{ number_format($cashflow) }}</b></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="5" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr>
                                     <td class="yfnc_d" colspan="5">
                                        <font color="orange">
                                        <strong>Operating Activities, Cash Flows Provided By or Used In</strong>
                                        </font>
                                    </td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Depreciation</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('depreciation');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Adjustments To Net Income</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('adjustments_to_net_income');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Accounts Receivables</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('changes_in_accounts_receivables');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Liabilities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('changes_in_liabilities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Inventories</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('changes_in_inventories');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Other Operating Activities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('changes_in_other_operating_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td colspan="2">

                                        <strong>
                                        Total Cash Flow From Operating Activities
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('total_cashflow_from_operating_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="5" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr>
                                    <td class="yfnc_d" colspan="5">
                                    <font color="orange">
                                    <strong>Investing Activities, Cash Flows Provided By or Used In</strong>
                                    </font>
                                    </td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Capital Expenditures</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('capital_expenditures');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Investments</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('investments');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Other Cash flows from Investing Activities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('other_cashflow_from_investing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td colspan="2">
                                        <strong>
                                        Total Cash Flows From Investing Activities
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('total_cashflow_from_investing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="6" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr>
                                    <td class="yfnc_d" colspan="5">
                                    <font color="orange">
                                    <strong>Financing Activities, Cash Flows Provided By or Used In</strong>
                                    </font>
                                    </td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Dividends Paid</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('dividends_paid');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Sale Purchase of Stock</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('sale_purchase_of_stock');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Net Borrowings</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('net_borrowings');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Other Cash Flows from Financing Activities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('other_cashflow_from_financing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td colspan="2">
                                        <strong>
                                        Total Cash Flows From Financing Activities
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('total_cashflow_from_financing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Effect Of Exchange Rate Changes</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('effect_of_exchange_rate_changes');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="6" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#006600">
                                     <td colspan="2">
                                        <strong>
                                        Change In Cash and Cash Equivalents
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 0)
                                         ->lists('change_in_cash_and_cash_equivalents');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                               </tbody>
                            </table>
                            </div>

                        </div>

                        <div id="content-tab-7" class="tab-pane">

                                <div align="center">
                                    <div class="ext-tabs" style="border: none;">
                                    <a href="#content-tab-6">Annual Data</a>/<a href="#content-tab-7">Quarterly Data</a>
                                    </div>
                                </div>

                            <div class="inner-padding">
                            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                               <tbody>
                                  <tr>
                                     <td class="yfnc_modtitle1" colspan="2">
                                     <span class="yfi-module-title"><strong>Period Ending</strong></span></td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('period_ending');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><b>{{ $cashflow }}</b></td>
                                         @endforeach
                                  </tr>
                                  <br/>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">

                                        <font color="orange"><strong>Net Income</strong></font>

                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('net_income');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><b>{{ number_format($cashflow) }}</b></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="5" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr>
                                     <td class="yfnc_d" colspan="5">
                                        <font color="orange">
                                        <strong>Operating Activities, Cash Flows Provided By or Used In</strong>
                                        </font>
                                    </td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Depreciation</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('depreciation');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Adjustments To Net Income</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('adjustments_to_net_income');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Accounts Receivables</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('changes_in_accounts_receivables');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Liabilities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('changes_in_liabilities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Inventories</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('changes_in_inventories');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Changes In Other Operating Activities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('changes_in_other_operating_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="5" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td colspan="2">

                                        <strong>
                                        Total Cash Flow From Operating Activities
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('total_cashflow_from_operating_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="5" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr>
                                    <td class="yfnc_d" colspan="5">
                                    <font color="orange">
                                    <strong>Investing Activities, Cash Flows Provided By or Used In</strong>
                                    </font>
                                    </td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Capital Expenditures</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('capital_expenditures');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Investments</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('investments');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Other Cash flows from Investing Activities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('other_cashflow_from_investing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="5" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td colspan="2">
                                        <strong>
                                        Total Cash Flows From Investing Activities
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('total_cashflow_from_investing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr>
                                     <td colspan="5" style="height:0;padding:0; "><span style="display:block; width:5px; height:10px;"></span></td>
                                  </tr>
                                  <tr>
                                    <td class="yfnc_d" colspan="5">
                                    <font color="orange">
                                    <strong>Financing Activities, Cash Flows Provided By or Used In</strong>
                                    </font>
                                    </td>
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Dividends Paid</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('dividends_paid');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Sale Purchase of Stock</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('sale_purchase_of_stock');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Net Borrowings</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('net_borrowings');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Other Cash Flows from Financing Activities</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('other_cashflow_from_financing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="5" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#404040">
                                     <td colspan="2">
                                        <strong>
                                        Total Cash Flows From Financing Activities
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('total_cashflow_from_financing_activities');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#333333">
                                     <td colspan="2">Effect Of Exchange Rate Changes</td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('effect_of_exchange_rate_changes');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right">{{ number_format($cashflow) }}</td>
                                         @endforeach
                                  </tr>
                                  <tr bgcolor="#bfbfbf">
                                     <td colspan="5" style="height:0;padding:0; border-top:3px solid #333;"><span style="display:block; width:5px; height:1px;"></span></td>
                                  </tr>
                                  <tr bgcolor="#006600">
                                     <td colspan="2">
                                        <strong>
                                        Change In Cash and Cash Equivalents
                                        </strong>
                                     </td>
                                        <?php
                                         $cashflows = DB::table('Financials_Cashflow')
                                         ->where('company_id', $company->id)
                                         ->where('sheet_type', 1)
                                         ->lists('change_in_cash_and_cash_equivalents');
                                         ?>

                                         @foreach ($cashflows as $cashflow)
                                         <td class="yfnc_modtitle1" align="right"><strong>{{ number_format($cashflow) }}</strong></td>
                                         @endforeach
                                  </tr>
                               </tbody>
                            </table>
                            </div>

                        </div>

                </div>
@stop